package frontend;

import Field.fullEffects;
import Field.sideEffects;
import backend.*;
import backend.DataAccess.DataAccessJSON;
import backend.DataAccess.iDataAccess;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    static iController SpielAngelegt = new Gamemaster();

    public void initialize(URL location, ResourceBundle resources) {
    }

    // @ToDo Radiobutton für regelwerk weil mehrfachauswahl und so
    // --------------------------------------------- Elemente für
    // scene_game-------------------------------------------------//
    @FXML
    GridPane Map;
    int cards = 1;
    @FXML
    ImageView card_1;
    @FXML
    ImageView card_2;
    @FXML
    ImageView card_3;
    @FXML
    ImageView card_4;
    @FXML
    ImageView card_5;
    @FXML
    ImageView card_6;
    @FXML
    ImageView card_7;
    @FXML
    ImageView card_8;
    @FXML
    ImageView card_9;
    @FXML
    Button ready;
    @FXML
    Button reset;
    @FXML
    Button power_down;
    @FXML
    ImageView player;
    @FXML
    ImageView life;
    @FXML
    ImageView robolifepic;
    @FXML
    private StackPane choose_1;
    @FXML
    private StackPane choose_2;
    @FXML
    private StackPane choose_3;
    @FXML
    private StackPane choose_4;
    @FXML
    private StackPane choose_5;
    @FXML
    private StackPane choose_6;
    @FXML
    private StackPane choose_7;
    @FXML
    private StackPane choose_8;
    @FXML
    private StackPane choose_9;
    @FXML
    Button move_forward;
    @FXML
    Button move_turn;
    @FXML
    Button next;
    @FXML
    StackPane robot1;
    StackPane robot2;
    StackPane robot3;
    StackPane robot4;
    StackPane robot5;
    StackPane robot6;
    StackPane robot7;
    StackPane robot8;
    @FXML
    Button play;
    ImageView image1111;
    File loadcsv;
    int z = 0;
    int y = 0;
    int a = 43;
    int b = 43;
    int c = 43;
    int d = 43;
    int e = 43;
    int f = 43;
    int g = 43;
    int h = 43;
    File loadjson;
    @FXML
    private VBox content;
    @FXML
    Label playerName;

    // --------------------------------------------- Elemente für
    // scene_start-------------------------------------------------//

    @FXML
    Button new_game;
    @FXML
    Button Beenden;
    @FXML
    Button SpielmodusAuswählen;

    // --------------------------------------------- Elemente für
    // addPlayerScene-------------------------------------------------//

    @FXML
    Button addPlayer;
    @FXML
    Label trueAdd;
    @FXML
    Label fileChooserpath;
    @FXML
    TextField spielername;
    @FXML
    Label playersadded;
    @FXML
    Button fileChooserButton;
    @FXML
    Button readyButton;
    @FXML
    Button CSVAuswahl;
    @FXML
    Button MapAuswahl;
    @FXML
    Label hoppla;
    @FXML
    CheckBox normal;
    @FXML
    CheckBox factoryRejects;
    @FXML
    CheckBox KIRobot;

    int players = 0;
    String path;

    // --------------------------------------------- Controller für
    // scene_game-------------------------------------------------//
    public void show_map() {

        try {
            iDataAccess json = new DataAccessJSON();

            Course course = (Course) json.load(path);

            for (int i = 0; i < course.getField()[0].length; i++) {
                for (int j = 0; j < course.getField().length; j++) {
                    File file;
                    ImageView image;
                    StackPane stackpane = new StackPane();
                    double width = 40;
                    double height = 40;
                    switch (course.getField()[j][i].getFullEffects()) {

                        case stand:
                            file = new File("src/frontend/pics/Muster-Standart.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case pit:
                            file = new File("src/frontend/pics/field_pit.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case flag1:
                            file = new File("src/frontend/pics/Muster-Standart.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            file = new File("src/frontend/pics/Flag1.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case flag2:
                            file = new File("src/frontend/pics/Muster-Standart.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            file = new File("src/frontend/pics/Flag2.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case flag3:
                            file = new File("src/frontend/pics/Muster-Standart.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            file = new File("src/frontend/pics/Flag3.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case flag4:
                            file = new File("src/frontend/pics/Muster-Standart.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            file = new File("src/frontend/pics/Flag4.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case dock1:
                            file = new File("src/frontend/pics/Dock1.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case dock2:
                            file = new File("src/frontend/pics/Dock2.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case dock3:
                            file = new File("src/frontend/pics/Dock3.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case dock4:
                            file = new File("src/frontend/pics/Dock4.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case dock5:
                            file = new File("src/frontend/pics/Dock5.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case dock6:
                            file = new File("src/frontend/pics/Dock6.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case dock7:
                            file = new File("src/frontend/pics/Dock7.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case dock8:
                            file = new File("src/frontend/pics/Dock8.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case belt_north:
                            file = new File("src/frontend/pics/Belt_North.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case belt_south:
                            file = new File("src/frontend/pics/Belt_South.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case belt_east:
                            file = new File("src/frontend/pics/Belt_East.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case belt_west:
                            file = new File("src/frontend/pics/Belt_West.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                        case repair:

                            file = new File("src/frontend/pics/Muster-Standart.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            file = new File("src/frontend/pics/Repair.png");
                            image = new ImageView(new Image(file.toURI().toString(), width, height, false, false));
                            stackpane.getChildren().addAll(image);
                            break;
                    }

                    if (course.getField()[j][i].getFullEffects() != fullEffects.pit) {
                        for (int z = 0; z < 4; z++) {
                            switch (z) {
                                case 0:
                                    if (course.getField()[j][i].getSideEffects()[z] != sideEffects.stand
                                            && (course.getField()[j][i].getSideEffects()[z] != null)) {
                                        if (course.getField()[j][i].getSideEffects()[z] == sideEffects.wall) {
                                            file = new File("src/frontend/pics/Wall_North.png");
                                            image = new ImageView(
                                                    new Image(file.toURI().toString(), width, height, false, false));
                                            stackpane.getChildren().addAll(image);
                                        } else if (course.getField()[j][i].getSideEffects()[z] == sideEffects.laser) {
                                            file = new File("src/frontend/pics/Wall_North.png");
                                            image = new ImageView(
                                                    new Image(file.toURI().toString(), width, height, false, false));
                                            stackpane.getChildren().addAll(image);
                                            file = new File("src/frontend/pics/laser_senkrecht.png");
                                            image = new ImageView(
                                                    new Image(file.toURI().toString(), width, height, false, false));
                                            stackpane.getChildren().addAll(image);
                                        }
                                    }
                                    break;
                                case 1:
                                    if (course.getField()[j][i].getSideEffects()[1] != sideEffects.stand
                                            && (course.getField()[j][i].getSideEffects()[1] != null)) {
                                        if (course.getField()[j][i].getSideEffects()[1] == sideEffects.wall) {
                                            file = new File("src/frontend/pics/Wall_East.png");
                                            image = new ImageView(
                                                    new Image(file.toURI().toString(), width, height, false, false));
                                            stackpane.getChildren().addAll(image);
                                        } else if (course.getField()[j][i].getSideEffects()[z] == sideEffects.laser) {
                                            file = new File("src/frontend/pics/Wall_East.png");
                                            image = new ImageView(
                                                    new Image(file.toURI().toString(), width, height, false, false));
                                            stackpane.getChildren().addAll(image);
                                            file = new File("src/frontend/pics/laser_horizontal.png");
                                            image = new ImageView(
                                                    new Image(file.toURI().toString(), width, height, false, false));
                                            stackpane.getChildren().addAll(image);
                                        }
                                    }
                                    break;
                                case 2:
                                    if (course.getField()[j][i].getSideEffects()[z] != sideEffects.stand
                                            && (course.getField()[j][i].getSideEffects()[z] != null)) {
                                        if (course.getField()[j][i].getSideEffects()[z] == sideEffects.wall) {
                                            file = new File("src/frontend/pics/Wall_South.png");
                                            image = new ImageView(
                                                    new Image(file.toURI().toString(), width, height, false, false));
                                            stackpane.getChildren().addAll(image);
                                        } else if (course.getField()[j][i].getSideEffects()[z] == sideEffects.laser) {
                                            file = new File("src/frontend/pics/Wall_South.png");
                                            image = new ImageView(
                                                    new Image(file.toURI().toString(), width, height, false, false));
                                            stackpane.getChildren().addAll(image);
                                            file = new File("src/frontend/pics/laser_senkrecht.png");
                                            image = new ImageView(
                                                    new Image(file.toURI().toString(), width, height, false, false));
                                            stackpane.getChildren().addAll(image);
                                        }
                                    }
                                    break;
                                case 3:
                                    if (course.getField()[j][i].getSideEffects()[z] != sideEffects.stand
                                            && (course.getField()[j][i].getSideEffects()[z] != null)) {
                                        if (course.getField()[j][i].getSideEffects()[z] == sideEffects.wall) {
                                            file = new File("src/frontend/pics/Wall_West.png");
                                            image = new ImageView(
                                                    new Image(file.toURI().toString(), width, height, false, false));
                                            stackpane.getChildren().addAll(image);
                                        } else if (course.getField()[j][i].getSideEffects()[z] == sideEffects.laser) {
                                            {
                                                file = new File("src/frontend/pics/Wall_West.png");
                                                image = new ImageView(
                                                        new Image(file.toURI().toString(), width, height, false, false));
                                                stackpane.getChildren().addAll(image);
                                                file = new File("src/frontend/pics/laser_horizontal.png");
                                                image = new ImageView(
                                                        new Image(file.toURI().toString(), width, height, false, false));
                                                stackpane.getChildren().addAll(image);
                                                // File classPathInput = new File(
                                                // ReadImageExample.class.getResource("laser_horizontal.png").getFile());
                                                // BufferedImage classpathImage = ImageIO.read(classPathInput);
                                            }
                                        }
                                        break;
                                    }
                            }
                        }
                    } // "file:src/frontend/Gui/Spielfiguren/Hammerbot.png"
                    // Image img = new
                    // Image("C:\\Users\\Lukas\\git\\RoboRally\\src\\frontend\\pics\\Belt_North.png");

                    Map.add(stackpane, i, j);
                }
                placeRobots();
                playerAnzeigeFull();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    public void openFileChooser1(ActionEvent event) {

        try {
            final FileChooser fileChooser = new FileChooser();
            // fileChooser.getExtensionFilters().addAll(new
            // FileChooser.ExtensionFilter("*.json"));
            File file = fileChooser.showOpenDialog(new Stage());
            path = file.getPath();
            show_map();
            show_card(event);
            reset.setDisable(false);
            ready.setDisable(false);
            // move_forward.setDisable(false);
            // move_turn.setDisable(false);
            power_down.setDisable(false);
            // next.setDisable(false);
        } catch (Exception io) {
            io.printStackTrace();
        }
    }

    @FXML
    public void chooseCards1() {
        File file;
        file = new File(cardsClicked());
        image1111 = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_1.getChildren().addAll(image1111);
        SpielAngelegt.chooseCard(0);
    }

    @FXML
    public void chooseCards2() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_2.getChildren().addAll(image);
        SpielAngelegt.chooseCard(1);
    }

    @FXML
    public void chooseCards3() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_3.getChildren().addAll(image);
        SpielAngelegt.chooseCard(2);
    }

    @FXML
    public void chooseCards4() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_4.getChildren().addAll(image);
        SpielAngelegt.chooseCard(3);
    }

    @FXML
    public void chooseCards5() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_5.getChildren().addAll(image);
        SpielAngelegt.chooseCard(4);
    }

    @FXML
    public void chooseCards6() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_6.getChildren().addAll(image);
        SpielAngelegt.chooseCard(5);
    }

    @FXML
    public void chooseCards7() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_7.getChildren().addAll(image);
        SpielAngelegt.chooseCard(6);
    }

    @FXML
    public void chooseCards8() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_8.getChildren().addAll(image);
        SpielAngelegt.chooseCard(7);
    }

    @FXML
    public void chooseCards9() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_9.getChildren().addAll(image);
        SpielAngelegt.chooseCard(8);// @ToDo Fehler beim Spielablauf: "ArrayIndexOutOfBoundsException: 8"

    }

    public void chooseCards1KI() {
        File file;
        file = new File(cardsClicked());
        image1111 = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_1.getChildren().addAll(image1111);
        System.out.println("1");
    }

    public void chooseCards2KI() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_2.getChildren().addAll(image);
        System.out.println("2");
    }

    public void chooseCards3KI() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_3.getChildren().addAll(image);
        System.out.println("3");
    }

    public void chooseCards4KI() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_4.getChildren().addAll(image);
        System.out.println("4");
    }

    public void chooseCards5KI() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_5.getChildren().addAll(image);
        System.out.println("5");
    }

    public void chooseCards6KI() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_6.getChildren().addAll(image);
        System.out.println("6");
    }

    public void chooseCards7KI() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_7.getChildren().addAll(image);
        System.out.println("7");
    }

    public void chooseCards8KI() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_8.getChildren().addAll(image);
        System.out.println("8");
    }

    public void chooseCards9KI() {
        File file;
        ImageView image;

        file = new File(cardsClicked());
        image = new ImageView(new Image(file.toURI().toString(), 60, 93, false, false));
        choose_9.getChildren().addAll(image);
        System.out.println("9");
    }

    @FXML
    public void resetCardsPicked() {
        if (choose_1.getChildren().size() > 1) {
            choose_1.getChildren().remove(1);
        }
        if (choose_2.getChildren().size() > 1) {
            choose_2.getChildren().remove(1);
        }
        if (choose_3.getChildren().size() > 1) {
            choose_3.getChildren().remove(1);
        }
        if (choose_4.getChildren().size() > 1) {
            choose_4.getChildren().remove(1);
        }
        if (choose_5.getChildren().size() > 1) {
            choose_5.getChildren().remove(1);
        }
        if (choose_6.getChildren().size() > 1) {
            choose_6.getChildren().remove(1);
        }
        if (choose_7.getChildren().size() > 1) {
            choose_7.getChildren().remove(1);
        }
        if (choose_8.getChildren().size() > 1) {
            choose_8.getChildren().remove(1);
        }
        if (choose_9.getChildren().size() > 1) {
            choose_9.getChildren().remove(1);
        }

        cards = 1;

    }

    public String cardsClicked() {
        switch (cards) {
            case 1:
                cards++;
                return "src/frontend/pics/1_choose.png";
            case 2:
                cards++;
                return "src/frontend/pics/2_choose.png";
            case 3:
                cards++;
                return "src/frontend/pics/3_choose.png";
            case 4:
                cards++;
                return "src/frontend/pics/4_choose.png";
            case 5:
                cards = 0;
                return "src/frontend/pics/5_choose.png";
            default:
                return "";
        }
    }

    public void deleteFromRegistry(ActionEvent event) {

    }

    public void rotateRobo(ActionEvent event) {
        // SpielAngelegt.setsActivePlayer();
        Robot activePlayer = SpielAngelegt.getActiveRobot();

        /**
         * WAT IS THIS ??????????????????????????????????????
         */
        /*
         * for(int i =0; i<10;i++) { Map.getChildren().remove(i,5); }
         */

        // SpielAngelegt.setsActivePlayer();
        // playerAnzeige();
        // Map.add(penisPane, activePlayer.getPositionY(), activePlayer.getPositionX());
        RotateTransition rt = null;

        switch (activePlayer.getColor()) {
            case "Pink":
                rt = new RotateTransition(javafx.util.Duration.millis(3000), robot1);
                rt.setByAngle(180);
                break;
            case "Blue":
                rt = new RotateTransition(javafx.util.Duration.millis(3000), robot2);
                rt.setByAngle(180);
                break;
            case "Green":
                rt = new RotateTransition(javafx.util.Duration.millis(3000), robot4);
                rt.setByAngle(180);
                break;
            case "Orange":
                rt = new RotateTransition(javafx.util.Duration.millis(3000), robot5);
                rt.setByAngle(180);
                break;
            case "White":
                rt = new RotateTransition(javafx.util.Duration.millis(3000), robot7);
                rt.setByAngle(180);
                break;
            case "Cyan":
                rt = new RotateTransition(javafx.util.Duration.millis(3000), robot3);
                rt.setByAngle(180);
                break;
            case "Yellow":
                rt = new RotateTransition(javafx.util.Duration.millis(3000), robot8);
                rt.setByAngle(180);
                break;
            case "Red":
                rt = new RotateTransition(javafx.util.Duration.millis(3000), robot6);
                rt.setByAngle(180);
                break;
            default:
                System.out.println("Schinken");
                break;
        }

        rt.play();
        // Map.getChildren().remove(robot1);

    }

    @FXML
    public void moveRobo() {
        Robot activePlayer = SpielAngelegt.getActiveRobot();
        // SpielAngelegt.setsActivePlayer();
        // playerAnzeige();
        TranslateTransition translateTransition = null;
        switch (activePlayer.getColor()) {
            case "Pink":
                translateTransition = new TranslateTransition(Duration.millis(3000), robot1);
                translateTransition.setToY(translateTransition.getByY() - a);
                a += 43;
                break;
            case "Blue":
                translateTransition = new TranslateTransition(Duration.millis(3000), robot2);
                translateTransition.setToY(translateTransition.getByY() - b);
                b += 43;
                break;
            case "Green":
                translateTransition = new TranslateTransition(Duration.millis(3000), robot4);
                translateTransition.setToY(translateTransition.getByY() - c);
                c += 43;
                break;
            case "Orange":
                translateTransition = new TranslateTransition(Duration.millis(3000), robot5);
                translateTransition.setToY(translateTransition.getByY() - d);
                d += 43;
                break;
            case "White":
                translateTransition = new TranslateTransition(Duration.millis(3000), robot7);
                translateTransition.setToY(translateTransition.getByY() - e);
                e += 43;
                break;
            case "Cyan":
                translateTransition = new TranslateTransition(Duration.millis(3000), robot3);
                translateTransition.setToY(translateTransition.getByY() - f);
                f += 43;
                break;
            case "Yellow":
                translateTransition = new TranslateTransition(Duration.millis(3000), robot8);
                translateTransition.setToY(translateTransition.getByY() - g);
                g += 43;
                break;
            case "Red":
                translateTransition = new TranslateTransition(Duration.millis(3000), robot6);
                translateTransition.setToY(translateTransition.getByY() - h);
                h += 43;
                break;
            default:
                System.out.println("Schinken");
                break;
        }
        String musicFile = "src/frontend/MoveBitch.mp3"; // For example

        Media sound = new Media(new File(musicFile).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.play();
        translateTransition.play();
    }

    @FXML
    public void playerAnzeige() {
        File file;
        Image image;
        Robot activePlayer = SpielAngelegt.getActiveRobot();
        switch (activePlayer.getColor()) {
            case "Pink":
                playerName.setText(activePlayer.getName());
                file = new File("src/frontend/pics/Robo_Pink.png");
                image = new Image(file.toURI().toString());
                player.setImage(image);
                break;
            case "Blue":
                playerName.setText(activePlayer.getName());
                file = new File("src/frontend/pics/Robo_Blue.png");
                image = new Image(file.toURI().toString());
                player.setImage(image);
                break;
            case "Green":
                playerName.setText(activePlayer.getName());
                file = new File("src/frontend/pics/Robo_Green.png");
                image = new Image(file.toURI().toString());
                player.setImage(image);
                break;
            case "Orange":
                playerName.setText(activePlayer.getName());
                file = new File("src/frontend/pics/Robo_Orange.png");
                image = new Image(file.toURI().toString());
                player.setImage(image);
                break;
            case "White":
                playerName.setText(activePlayer.getName());
                file = new File("src/frontend/pics/Robo_White.png");
                image = new Image(file.toURI().toString());
                player.setImage(image);
                break;
            case "Cyan":
                playerName.setText(activePlayer.getName());
                file = new File("src/frontend/pics/Robo_Cyan.png");
                image = new Image(file.toURI().toString());
                player.setImage(image);
                break;
            case "Yellow":
                playerName.setText(activePlayer.getName());
                file = new File("src/frontend/pics/Robo_Yellow.png");
                image = new Image(file.toURI().toString());
                player.setImage(image);
                break;
            case "Red":
                playerName.setText(activePlayer.getName());
                file = new File("src/frontend/pics/Robo_red.png");
                image = new Image(file.toURI().toString());
                player.setImage(image);
                break;
            default:
                System.out.println("Schinken");
                break;
        }

    }

    @FXML
    public void next() {
        SpielAngelegt.setsActivePlayer();
        playerAnzeigeFull();
    }

    public void powerDown() {
        if (SpielAngelegt.powerDown()) {
            File file;
            Image image;
            ImageView img = new ImageView();
            Robot activePlayer = SpielAngelegt.getActiveRobot();
            switch (activePlayer.getColor()) {
                case "Pink":
                    file = new File("src/frontend/pics/Robo_Disabled.png");
                    image = new Image(file.toURI().toString());
                    img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                    robot1.getChildren().add(img);
                    break;
                case "Blue":
                    file = new File("src/frontend/pics/Robo_Disabled.png");
                    image = new Image(file.toURI().toString());
                    img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                    robot2.getChildren().add(img);
                    break;
                case "Green":
                    file = new File("src/frontend/pics/Robo_Disabled.png");
                    image = new Image(file.toURI().toString());
                    img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                    robot4.getChildren().add(img);
                    break;
                case "Orange":
                    file = new File("src/frontend/pics/Robo_Disabled.png");
                    image = new Image(file.toURI().toString());
                    img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                    robot5.getChildren().add(img);
                    break;
                case "White":
                    file = new File("src/frontend/pics/Robo_Disabled.png");
                    image = new Image(file.toURI().toString());
                    img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                    robot7.getChildren().add(img);
                    break;
                case "Cyan":
                    file = new File("src/frontend/pics/Robo_Disabled.png");
                    image = new Image(file.toURI().toString());
                    img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                    robot3.getChildren().add(img);
                    break;
                case "Yellow":
                    file = new File("src/frontend/pics/Robo_Disabled.png");
                    image = new Image(file.toURI().toString());
                    img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                    robot8.getChildren().add(img);
                    break;
                case "Red":
                    file = new File("src/frontend/pics/Robo_Disabled.png");
                    image = new Image(file.toURI().toString());
                    img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                    robot6.getChildren().add(img);
                    break;
                default:
                    System.out.println("Schinken");
                    break;
            }
        }
    }

    public void show_card(ActionEvent event) {

        String[] playercards = SpielAngelegt.showCards();

        File file = null;
        Image image = null;
        for (int i = 0; i < playercards.length; i++) {
            System.out.print(playercards[i] + " ");
        }
        for (int i = 0; i < playercards.length; i++) {
            if (playercards[i].endsWith("move1")) {
                if (playercards[i].startsWith("490"))
                    file = new File("src/frontend/pics/490.png");
                else if (playercards[i].startsWith("500"))
                    file = new File("src/frontend/pics/500.png");
                else if (playercards[i].startsWith("510"))
                    file = new File("src/frontend/pics/510.png");
                else if (playercards[i].startsWith("520"))
                    file = new File("src/frontend/pics/520.png");
                else if (playercards[i].startsWith("530"))
                    file = new File("src/frontend/pics/530.png");
                else if (playercards[i].startsWith("540"))
                    file = new File("src/frontend/pics/540.png");
                else if (playercards[i].startsWith("550"))
                    file = new File("src/frontend/pics/550.png");
                else if (playercards[i].startsWith("560"))
                    file = new File("src/frontend/pics/560.png");
                else if (playercards[i].startsWith("570"))
                    file = new File("src/frontend/pics/570.png");
                else if (playercards[i].startsWith("580"))
                    file = new File("src/frontend/pics/580.png");
                else if (playercards[i].startsWith("590"))
                    file = new File("src/frontend/pics/590.png");
                else if (playercards[i].startsWith("600"))
                    file = new File("src/frontend/pics/600.png");
                else if (playercards[i].startsWith("610"))
                    file = new File("src/frontend/pics/610.png"); // @ToDo Karte fehlt
                else if (playercards[i].startsWith("620"))
                    file = new File("src/frontend/pics/620.png");
                else if (playercards[i].startsWith("630"))
                    file = new File("src/frontend/pics/630.png");
                else if (playercards[i].startsWith("640"))
                    file = new File("src/frontend/pics/640.png");
                else if (playercards[i].startsWith("650"))
                    file = new File("src/frontend/pics/650.png");
                else if (playercards[i].startsWith("660"))
                    file = new File("src/frontend/pics/660.png");

                image = new Image(file.toURI().toString(), 80, 124, false, false);
            }
            if (playercards[i].endsWith("move2")) {
                if (playercards[i].startsWith("670"))
                    file = new File("src/frontend/pics/670.png");
                else if (playercards[i].startsWith("680"))
                    file = new File("src/frontend/pics/680.png");
                else if (playercards[i].startsWith("690"))
                    file = new File("src/frontend/pics/690.png");
                else if (playercards[i].startsWith("700"))
                    file = new File("src/frontend/pics/700.png");
                else if (playercards[i].startsWith("710"))
                    file = new File("src/frontend/pics/710.png");
                else if (playercards[i].startsWith("720"))
                    file = new File("src/frontend/pics/720.png");
                else if (playercards[i].startsWith("730"))
                    file = new File("src/frontend/pics/730.png");
                else if (playercards[i].startsWith("740"))
                    file = new File("src/frontend/pics/740.png");
                else if (playercards[i].startsWith("750"))
                    file = new File("src/frontend/pics/750.png");
                else if (playercards[i].startsWith("760"))
                    file = new File("src/frontend/pics/760.png");
                else if (playercards[i].startsWith("770"))
                    file = new File("src/frontend/pics/770.png");
                else if (playercards[i].startsWith("780"))
                    file = new File("src/frontend/pics/780.png");

                image = new Image(file.toURI().toString(), 80, 124, false, false);
            }
            if (playercards[i].endsWith("move3")) {
                if (playercards[i].startsWith("790"))
                    file = new File("src/frontend/pics/790.png");
                else if (playercards[i].startsWith("800"))
                    file = new File("src/frontend/pics/800.png");
                else if (playercards[i].startsWith("810"))
                    file = new File("src/frontend/pics/810.png");
                else if (playercards[i].startsWith("820"))
                    file = new File("src/frontend/pics/820.png");
                else if (playercards[i].startsWith("830"))
                    file = new File("src/frontend/pics/830.png");

                else if (playercards[i].startsWith("840"))
                    file = new File("src/frontend/pics/840.png");

                image = new Image(file.toURI().toString(), 80, 124, false, false);

            }
            if (playercards[i].endsWith("UTurn")) {
                if (playercards[i].startsWith("10"))
                    file = new File("src/frontend/pics/10.png");
                else if (playercards[i].startsWith("20"))
                    file = new File("src/frontend/pics/20.png"); // @ToDo Zahl fehlt...
                else if (playercards[i].startsWith("30"))
                    file = new File("src/frontend/pics/30.png");
                else if (playercards[i].startsWith("40"))
                    file = new File("src/frontend/pics/40.png");
                else if (playercards[i].startsWith("50"))
                    file = new File("src/frontend/pics/50.png");

                else if (playercards[i].startsWith("60"))
                    file = new File("src/frontend/pics/60.png");

                image = new Image(file.toURI().toString(), 80, 124, false, false);
            }
            if (playercards[i].endsWith("RotateLeft")) {
                if (playercards[i].startsWith("410"))
                    file = new File("src/frontend/pics/410.png");
                else if (playercards[i].startsWith("390"))
                    file = new File("src/frontend/pics/390.png");
                else if (playercards[i].startsWith("370"))
                    file = new File("src/frontend/pics/370.png");
                else if (playercards[i].startsWith("350"))
                    file = new File("src/frontend/pics/350.png");
                else if (playercards[i].startsWith("330"))
                    file = new File("src/frontend/pics/330.png");

                else if (playercards[i].startsWith("310"))
                    file = new File("src/frontend/pics/310.png");
                else if (playercards[i].startsWith("290"))
                    file = new File("src/frontend/pics/290.png");
                else if (playercards[i].startsWith("270"))
                    file = new File("src/frontend/pics/270.png");
                else if (playercards[i].startsWith("250"))
                    file = new File("src/frontend/pics/250.png");
                else if (playercards[i].startsWith("230"))
                    file = new File("src/frontend/pics/230.png");
                else if (playercards[i].startsWith("210"))
                    file = new File("src/frontend/pics/210.png");

                else if (playercards[i].startsWith("190"))
                    file = new File("src/frontend/pics/190.png");
                else if (playercards[i].startsWith("170"))
                    file = new File("src/frontend/pics/170.png");
                else if (playercards[i].startsWith("150"))
                    file = new File("src/frontend/pics/150.png");
                else if (playercards[i].startsWith("130"))
                    file = new File("src/frontend/pics/130.png");
                else if (playercards[i].startsWith("110"))
                    file = new File("src/frontend/pics/110.png");
                else if (playercards[i].startsWith("90"))
                    file = new File("src/frontend/pics/90.png");
                else if (playercards[i].startsWith("70"))
                    file = new File("src/frontend/pics/70.png");
                image = new Image(file.toURI().toString(), 80, 124, false, false);
            }
            if (playercards[i].endsWith("RotateRight")) {
                if (playercards[i].startsWith("420"))
                    file = new File("src/frontend/pics/420.png");
                else if (playercards[i].startsWith("400"))
                    file = new File("src/frontend/pics/400.png");
                else if (playercards[i].startsWith("380"))
                    file = new File("src/frontend/pics/380.png");
                else if (playercards[i].startsWith("360"))
                    file = new File("src/frontend/pics/360.png");
                else if (playercards[i].startsWith("340"))
                    file = new File("src/frontend/pics/340.png");

                else if (playercards[i].startsWith("320"))
                    file = new File("src/frontend/pics/320.png");

                else if (playercards[i].startsWith("300"))
                    file = new File("src/frontend/pics/300.png");
                else if (playercards[i].startsWith("280"))
                    file = new File("src/frontend/pics/280.png");
                else if (playercards[i].startsWith("260"))
                    file = new File("src/frontend/pics/260.png"); // @ToDo Karte Fehlt
                else if (playercards[i].startsWith("240"))
                    file = new File("src/frontend/pics/240.png");
                else if (playercards[i].startsWith("220"))
                    file = new File("src/frontend/pics/220.png");

                else if (playercards[i].startsWith("200"))
                    file = new File("src/frontend/pics/200.png");
                else if (playercards[i].startsWith("180"))
                    file = new File("src/frontend/pics/180.png");
                else if (playercards[i].startsWith("160"))
                    file = new File("src/frontend/pics/160.png");
                else if (playercards[i].startsWith("140"))
                    file = new File("src/frontend/pics/140.png");
                else if (playercards[i].startsWith("120"))
                    file = new File("src/frontend/pics/120.png");
                else if (playercards[i].startsWith("100"))
                    file = new File("src/frontend/pics/100.png");
                else if (playercards[i].startsWith("80"))
                    file = new File("src/frontend/pics/80.png");
                image = new Image(file.toURI().toString(), 80, 124, false, false);
            }
            if (playercards[i].endsWith("Backup")) {
                if (playercards[i].startsWith("430"))
                    file = new File("src/frontend/pics/430.png");
                else if (playercards[i].startsWith("440"))
                    file = new File("src/frontend/pics/440.png");
                else if (playercards[i].startsWith("450"))
                    file = new File("src/frontend/pics/450.png");
                else if (playercards[i].startsWith("460"))
                    file = new File("src/frontend/pics/460.png");
                else if (playercards[i].startsWith("470"))
                    file = new File("src/frontend/pics/470.png");
                else if (playercards[i].startsWith("480"))
                    file = new File("src/frontend/pics/480.png");
                image = new Image(file.toURI().toString(), 80, 124, false, false);

            }
            if (i == 0)
                card_1.setImage(image);
            else if (i == 1)
                card_2.setImage(image);
            else if (i == 2)
                card_3.setImage(image);
            else if (i == 3)
                card_4.setImage(image);
            else if (i == 4)
                card_5.setImage(image);
            else if (i == 5)
                card_6.setImage(image);
            else if (i == 6)
                card_7.setImage(image);
            else if (i == 7)
                card_8.setImage(image);
            else if (i == 8)
                card_9.setImage(image);
        }
        // card_1.setImage(image);
    }

    public void placeRobots() {
        File file = null;
        Image image;

        Robot activePlayer = SpielAngelegt.getActiveRobot();
        Ringbuffer playerbuffer = SpielAngelegt.getRoundBuffer();
        playerbuffer.resetRead();

        for (int i = -1; i < playerbuffer.getCapacity(); i++) {
            activePlayer = (Robot) playerbuffer.peek();
            int y = activePlayer.getPositionX();
            int x = activePlayer.getPositionY();

            if (activePlayer.getColor().equals("Pink")) {
                file = new File("src/frontend/pics/Robo_Pink.png");
                // image = new Image(file.toURI().toString());
                robot1 = new StackPane();
                ImageView img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                robot1.getChildren().addAll(img);
                Map.add(robot1, y, x);
            } else if (activePlayer.getColor().equals("Blue")) {
                file = new File("src/frontend/pics/Robo_Blue.png");
                image = new Image(file.toURI().toString());
                robot2 = new StackPane();
                ImageView img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                robot2.getChildren().addAll(img);
                Map.add(robot2, y, x);
            } else if (activePlayer.getColor().equals("Cyan")) {
                file = new File("src/frontend/pics/Robo_Cyan.png");
                image = new Image(file.toURI().toString());
                robot3 = new StackPane();
                ImageView img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                robot3.getChildren().addAll(img);
                Map.add(robot3, y, x);
            } else if (activePlayer.getColor().equals("Green")) {
                file = new File("src/frontend/pics/Robo_Green.png");
                image = new Image(file.toURI().toString());
                robot4 = new StackPane();
                ImageView img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                robot4.getChildren().addAll(img);
                Map.add(robot4, y, x);
            } else if (activePlayer.getColor().equals("Orange")) {
                file = new File("src/frontend/pics/Robo_Orange.png");
                image = new Image(file.toURI().toString());
                robot5 = new StackPane();
                ImageView img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                robot5.getChildren().addAll(img);
                Map.add(robot5, y, x);
            } else if (activePlayer.getColor().equals("Red")) {
                file = new File("src/frontend/pics/Robo_Red.png");
                image = new Image(file.toURI().toString());
                robot6 = new StackPane();
                ImageView img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                robot6.getChildren().addAll(img);
                Map.add(robot6, y, x);
            } else if (activePlayer.getColor().equals("White")) {
                file = new File("src/frontend/pics/Robo_White.png");
                image = new Image(file.toURI().toString());
                robot7 = new StackPane();
                ImageView img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                robot7.getChildren().addAll(img);
                Map.add(robot7, y, x);
            } else if (activePlayer.getColor().equals("Yellow")) {
                file = new File("src/frontend/pics/Robo_Yellow.png");
                image = new Image(file.toURI().toString());
                robot8 = new StackPane();
                ImageView img = new ImageView(new Image(file.toURI().toString(), 35, 35, false, false));
                robot8.getChildren().addAll(img);
                Map.add(robot8, y, x);
            }

        }

    }

    public void showCurrentPlayerLife() {
        Robot activePlayer = SpielAngelegt.getActiveRobot();
        int playerlife = activePlayer.getPlayerlife();
        int robolife = activePlayer.getRobolife();
        File file = new File("src/frontend/pics/Full_Life.png");
        Image image = new Image(file.toURI().toString());
        switch (playerlife) {
            case 0:
                file = new File("src/frontend/pics/No_Life.png");
                image = new Image(file.toURI().toString());
                break;
            case 1:
                file = new File("src/frontend/pics/1_Life.png");
                image = new Image(file.toURI().toString());
                break;
            case 2:
                file = new File("src/frontend/pics/2_Life.png");
                image = new Image(file.toURI().toString());
                break;
            case 3:
                file = new File("src/frontend/pics/Full_Life.png");
                image = new Image(file.toURI().toString());
                break;
        }

        File file1 = new File("src/frontend/pics/Full_RoboLife.png");
        Image image1 = new Image(file.toURI().toString());
        switch (robolife) {
            case 0:
                file1 = new File("src/frontend/pics/No_RoboLife.png");
                image1 = new Image(file1.toURI().toString());
                break;
            case 1:
                file1 = new File("src/frontend/pics/1_RoboLife.png");
                image1 = new Image(file1.toURI().toString());
                break;
            case 2:
                file1 = new File("src/frontend/pics/2_RoboLife.png");
                image1 = new Image(file1.toURI().toString());
                break;
            case 3:
                file1 = new File("src/frontend/pics/3_RoboLife.png");
                image1 = new Image(file1.toURI().toString());
                break;
            case 4:
                file1 = new File("src/frontend/pics/4_RoboLife.png");
                image1 = new Image(file1.toURI().toString());
                break;
            case 5:
                file1 = new File("src/frontend/pics/5_RoboLife.png");
                image1 = new Image(file1.toURI().toString());
                break;
            case 6:
                file1 = new File("src/frontend/pics/6_RoboLife.png");
                image1 = new Image(file1.toURI().toString());
                break;
            case 7:
                file1 = new File("src/frontend/pics/7_RoboLife.png");
                image1 = new Image(file1.toURI().toString());
                break;
            case 8:
                file1 = new File("src/frontend/pics/8_RoboLife.png");
                image1 = new Image(file1.toURI().toString());
                break;
            case 9:
                file1 = new File("src/frontend/pics/9_RoboLife.png");
                image1 = new Image(file1.toURI().toString());
                break;
            default:
                file1 = new File("src/frontend/pics/Full_RoboLife.png");
                image1 = new Image(file1.toURI().toString());
                break;
        }
        life.setImage(image);
        robolifepic.setImage(image1);
        // SpielAngelegt.getActiveRobot().setPlayerlife(1);
        // SpielAngelegt.getActiveRobot().setRobolife(robolife-2);
    }

    public void OpenScene_Laden(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/scene_laden.fxml"));
            Stage stage = (Stage) new_game.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            show_card(event);
            stage.setScene(scene);
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    // @ToDo aktuell wird nur ein Register ausgef�hrt, dann Dauerschleife f�r
    // die
    // Kartenauswahl
    public void registryReady(ActionEvent event) throws InterruptedException {
        resetCardsPicked();
        reset.setDisable(false);
        SpielAngelegt.setRegistryReady();

        Robot neu = SpielAngelegt.getActiveRobot();
        playerAnzeigeFull();
        if (neu instanceof RobotKI) {
            reset.setDisable(true);
            ((RobotKI) neu).chooseCardKI();

            int[] handki = neu.getHand();
            int[] regki = neu.getRegistry();

            System.out.println();
            for (int i = 0; i < handki.length; i++) {
                System.out.print(handki[i] + " ");
            }
            System.out.println();
            resetCardsPicked();
            for (int i = 0; i < regki.length; i++) {
                for (int j = 0; j < handki.length + 1; j++) {
                    if (regki[i] == handki[0]) {
                        chooseCards1KI();
                        break;
                    } else if (regki[i] == handki[1]) {
                        chooseCards2KI();
                        break;
                    } else if (regki[i] == handki[2]) {
                        chooseCards3KI();
                        break;
                    } else if (regki[i] == handki[3]) {
                        chooseCards4KI();
                        break;
                    } else if (regki[i] == handki[4]) {
                        chooseCards5KI();
                        break;
                    } else if (regki[i] == handki[5]) {
                        chooseCards6KI();
                        break;
                    } else if (regki[i] == handki[6]) {
                        chooseCards7KI();
                        break;
                    } else if (regki[i] == handki[7]) {
                        chooseCards8KI();
                        break;
                    } else if (regki[i] == handki[8]) {
                        chooseCards9KI();
                        break;
                    }
                }
            }
            // playerAnzeigeFull();
        }
        System.out.println("-----Karten ausgewählt-----");

        playerAnzeigeFull();

        show_card(event);

        everyoneRegistryReady();
    }

    public void playerAnzeigeFull() {
        playerAnzeige();
        showCurrentPlayerLife();
    }

    public void everyoneRegistryReady() {
        Robot robot = null;
        int anzahl = 0;
        for (int i = 0; i < SpielAngelegt.getRoundBuffer().getCapacity(); i++) {
            robot = (Robot) SpielAngelegt.getRoundBuffer().peek();

            if (robot.getRegistryReady()) {
                anzahl++;

            }
        }
        if (anzahl == SpielAngelegt.getRoundBuffer().getCapacity()) {

            SpielAngelegt.playRound();
            placeRobots();
            SpielAngelegt.resetActivePlayer();
            anzahl = 0;
        }
    }

    public void popup(ActionEvent event) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/popupMenu.fxml"));
        try {
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setX(400);
            stage.show();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    // --------------------------------------------- Controller für
    // scene_laden-------------------------------------------------//

    public void OpenCSV(ActionEvent event) {
        try {
            final FileChooser fileChooser = new FileChooser();
            loadcsv = fileChooser.showOpenDialog(new Stage());

        } catch (Exception io) {
            io.printStackTrace();
        }
    }

    public void OpenJSON(ActionEvent event) {
        try {
            final FileChooser fileChooser = new FileChooser();
            loadjson = fileChooser.showOpenDialog(new Stage());
            // fileChooserpath.setText(file.getName());
        } catch (Exception io) {
            io.printStackTrace();
        }
    }

    public void play(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/scene_game.fxml"));
            Stage stage = (Stage) play.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            show_card(event);
            stage.setScene(scene);
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    // --------------------------------------------- Controller für
    // addPlayerScene-------------------------------------------------//
    @FXML
    public void addPlayer(ActionEvent event) {
        // VBox FileChoose = new VBox();
        // FileChoose.getChildren().addAll(fileChooserButton, fileChooserpath);

        try {
            boolean ki = false;
            String text = spielername.getText();
            if (KIRobot.isSelected()) {
                ki = true;
            }
            if (SpielAngelegt.addPlayer(text, ki) == true) {

                trueAdd.setText("Spieler " + text + " wurde in den Playerbuffer hinzugef�gt");
                playersadded.setText("Es wurden bisher " + ++players + " hinzugef�gt");
            }
            ;

        } catch (Exception io) {
            io.printStackTrace();
        }
    }

    @FXML
    public void openFileChooser(ActionEvent event) {

        try {
            final FileChooser fileChooser = new FileChooser();
            // fileChooser.getExtensionFilters().addAll(new
            // FileChooser.ExtensionFilter("*.json"));
            File file = fileChooser.showOpenDialog(new Stage());

            fileChooserpath.setText(file.getName());

        } catch (Exception io) {
            io.printStackTrace();
        }
    }

    @FXML
    public void ready(ActionEvent event) {
        if (factoryRejects.isSelected()) {
            defaultRulebook rulebook = new RuleFactoryRejects();
            SpielAngelegt.setRules(rulebook);
        }
        if (SpielAngelegt.startGame() == "") {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/scene_game.fxml"));
                Stage stage = (Stage) readyButton.getScene().getWindow();
                Scene scene = new Scene(loader.load());
                stage.setScene(scene);
            } catch (IOException io) {
                io.printStackTrace();
            }
        } else {
            hoppla.setText("Hoppla du bist schei�e!");
        }
    }

    // --------------------------------------------- Controller für
    // scene_start-------------------------------------------------//

    @FXML
    public void close(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    public void OpenAddPlayerScene(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/addPlayerScene.fxml"));
            Stage stage = (Stage) new_game.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
        } catch (IOException io) {
            io.printStackTrace();
        }

    }

    @FXML
    public void OpenAddPlayerScene2(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/SpielmodusLaden.fxml"));
            Stage stage = (Stage) new_game.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
        } catch (IOException io) {
            io.printStackTrace();
        }

    }

}