package frontend;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class GraphicUserInterface extends javafx.application.Application {

    public GraphicUserInterface() {
        super();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("fxml/scene_start.fxml"));
            Scene scene = new Scene(root, 600, 800);
            //@ToDo Name von Spieler unter Bild des Roboters ausgeben
            //@ToDo Gesammelte Flaggen anzeigen
            //@ToDo Menü (Beenden, Resetten, Speichern, Laden? und mit was das ganze?)
            primaryStage.setTitle("RoboRally Start");
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
