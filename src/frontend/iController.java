package frontend;

import backend.Ringbuffer;
import backend.Robot;

public interface iController {

    /**
     * addPlayer erstellt ein Roboter-Object & f�gt einen Spieler im Playerbuffer hinzu
     *
     * @return boolean, ob das anlegen geklappt hat
     */
    public boolean addPlayer(String name, boolean ki);

    /**
     * Bereitet RoundBuffer vor und pr�ft, ob gen�gend Spieler angemeldet sind.
     * Ruft die Methode placeRobots(), deck() & dealCardsNewRound() auf.
     *
     * @return String, Fehlermeldung anzeigen oder null
     */
    public String startGame();

    public void playRound();

    /**
     * powerdown bestimmt, ob der Spieler seinen Roboter eine Runde aussetzen l�sst
     *
     * @return boolean, ob das aussetzen klappt
     */
    public boolean powerDown();

    /**
     * der Spieler w�hlt eine Karte aus, die dann an die passende Regritry-Indexstelle eingetragen wird
     *
     * @return boolean, ob Power gerade on oder off ist
     */
    public boolean chooseCard(int index);

    /**
     * Zeigt die aktuelle Map an. '>' '<' '^' 'v' (Belt), 'o' (Dock), 'x' (Pit), 'f' (Flag), '#' (Standard)
     *
     * @return String[][]
     */
    public String[][] showMap();

    /**
     * Killt alles.
     *
     * @return void
     */
    public void exitGame();

    /**
     * holt sich das aktuelle hand[] des Spielers und zeigt die Karten an.
     *
     * @return String[], für Information der Karte
     */
    public String[] showCards();

    /**
     * zeigt das akutelle Leben des Roboters an, der gerade an der Reihe ist
     *
     * @return Int aktuelles Leben
     */
    public int showRobolife();

    /**
     * zeigt an, wie oft der Roboter gerespawnt werden kann
     *
     * @return Int m�gliche Anzahl respawns
     */
    public int showPlayerlife();

    /**
     * Gibt den Namen und die Farbe des am zug befindenden Roboters zurück-
     *
     * @return String Array mit Name und Farbe
     */
    public String[] getPlayernameAndColor();

    public String[] isFinished();

    /**
     * Erfragt, wie viele Karten ausgew�hlt werden k�nnen.
     * @return int 
     */
    public int getRegistryLength();

    public void resetActivePlayer();

    public Ringbuffer getRoundBuffer();

    public Robot getActiveRobot();

    public void setsActivePlayer();

    public boolean addPlayer(Robot robot);

    public int[] showRegistry();
    
    public void setRegistryReady();
    
    public boolean getRegistryReady();
    
    public void setRules(Object rulebook);
    
    //public void chooseCardKI();
    
}
