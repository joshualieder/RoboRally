package Field;

public class Field {

    private sideEffects[] sideEffects = new sideEffects[4];
    private fullEffects fullEffects;
 
    /**
     * Sideeffects (Wall, Laser oder Stand) werden in ein Array gesetzt.
     * Somit kann man ausgehen an welcher Seite sich die Objekte befinden.
     *
     * @param sideEffect_north
     * @param sideEffect_east
     * @param sideEffect_south
     * @param sideEffect_west
     * @param fullEffect
     */ 


    public Field(sideEffects sideEffect_north, sideEffects sideEffect_east, sideEffects sideEffect_south, sideEffects sideEffect_west, fullEffects fullEffect) {
        sideEffects[0] = sideEffect_north;//Nord-Objekt
        sideEffects[1] = sideEffect_east;//East-Objekt
        sideEffects[2] = sideEffect_south;//South-Objekt
        sideEffects[3] = sideEffect_west;//West-Objekt
        setFullEffects(fullEffect);
    }

    public sideEffects[] getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(sideEffects[] sideEffects) {
        this.sideEffects = sideEffects;
    }

    public fullEffects getFullEffects() {
        return fullEffects;
    }

    public void setFullEffects(fullEffects fullEffects) {
        this.fullEffects = fullEffects;
    }

    public void fieldaction() {
    }

}
