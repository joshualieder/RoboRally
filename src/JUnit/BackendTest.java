package JUnit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;

import backend.Gamemaster;
import backend.Ringbuffer;
import backend.RuleCheckmate;
import backend.defaultRulebook;
import frontend.iController;

public class BackendTest {
	defaultRulebook rules = new RuleCheckmate();
	iController startGame = new Gamemaster(rules);
	
	
	@Before
	public void setPlayers(){
		Assert.assertTrue(startGame.addPlayer("Spieler1",false));
		Assert.assertTrue(startGame.addPlayer("Spieler2",false));
		Assert.assertTrue(startGame.addPlayer("Spieler3",false));
		Assert.assertTrue(startGame.addPlayer("Spieler4",false));
		Assert.assertTrue(startGame.addPlayer("Spieler5",false));	
	}
	
	@Test
	public void beginGame() {
		Assert.assertEquals(null, startGame.startGame());
	}
	
	@Test
	public void showFirstGamepic() {
		Assert.assertEquals(" x  x  x  x  x  x  x  x  x  x  x  x  x  x \r\n" + 
				" x  #  #  #  #  #  #  #  #  #  #  #  @  x \r\n" + 
				" x  #  >  >  >  >  >  >  >  >  >  v  #  x \r\n" + 
				" x  #  ^  >  #  >  #  <  1  <  #  v  #  x \r\n" + 
				" x  #  ^  #  x  #  >  #  <  #  <  v  #  x \r\n" + 
				" x  #  ^  >  #  >  #  x  #  <  #  v  #  x \r\n" + 
				" x  #  ^  #  >  #  @  #  <  #  <  v  #  x \r\n" + 
				" x  #  ^  >  #  >  #  @  #  x  #  v  #  x \r\n" + 
				" x  #  ^  #  >  #  x  #  <  #  <  v  #  x \r\n" + 
				" x  #  ^  >  2  >  #  <  #  <  #  v  #  x \r\n" + 
				" x  #  ^  #  >  #  >  #  <  #  <  v  #  x \r\n" + 
				" x  #  ^  <  <  <  <  <  <  <  <  <  #  x \r\n" + 
				" x  @  #  #  #  #  #  #  #  #  #  #  #  x \r\n" + 
				" x  #  #  #  #  #  #  #  #  #  #  #  #  x \r\n" + 
				" x  #  #  #  #  #  #  #  #  #  #  #  #  x \r\n" + 
				" x  ~  W  #  G  #  P  B  #  O  #  ~  ~  x \r\n" + 
				" x  #  #  #  #  #  #  #  #  #  #  #  #  x \r\n" + 
				" x  x  x  x  x  x  x  x  x  x  x  x  x  x \r\n"  
				, startGame.showMap());
	}
	
	@Test
	public void expectedPlayerSort() {
		Ringbuffer actualPlayerSort = startGame.getRoundBuffer();
		actualPlayerSort.peek();
		Assert.assertSame("Spieler1","Pink", startGame.getPlayernameAndColor());
		actualPlayerSort.peek();
		Assert.assertSame("Spieler2","Blue", startGame.getPlayernameAndColor());
		actualPlayerSort.peek();
		Assert.assertSame("Spieler3","Green", startGame.getPlayernameAndColor());
		actualPlayerSort.peek();
		Assert.assertSame("Spieler4","Orange", startGame.getPlayernameAndColor());
		actualPlayerSort.peek();
		Assert.assertSame("Spieler5","White", startGame.getPlayernameAndColor());
		actualPlayerSort.peek();
		Assert.assertSame("Spieler1","Pink", startGame.getPlayernameAndColor());
	}
	
	@Test
	public void setRegistry() {
		for (int i = 0; i < startGame.getRegistryLength(); i++) {
			Assert.assertTrue(startGame.chooseCard(i));
		}
	}
}
