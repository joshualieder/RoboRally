package JUnit;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import backend.DataAccess.DataAccessSER;
import backend.Robot;
import frontend.iController;
import org.junit.jupiter.api.Test;

import backend.DataAccess.iDataAccess;
import backend.Gamemaster;

public class SaveSERTest {

    private static iController Spielleiter = new Gamemaster();

    @Test
    public void testSave() throws FileNotFoundException, IOException {

        Spielleiter.addPlayer("k",false);
        Spielleiter.addPlayer("f",false);
        Spielleiter.addPlayer("as",false);
        Spielleiter.addPlayer("asadf",false);
        Spielleiter.addPlayer("aqga",false);

        Spielleiter.startGame();

        iDataAccess bitch = new DataAccessSER();

        bitch.save(Spielleiter.getRoundBuffer(), "test.bitch");
        Object tmp_object = bitch.load("test.bitch");

        HashMap<Integer, Robot> saved = (HashMap<Integer, Robot>) tmp_object;

        for (int i = 0; i < saved.size(); i++) {
            Robot tmp = saved.get(i);
            Spielleiter.getRoundBuffer().push(tmp);
        }

    }
}
