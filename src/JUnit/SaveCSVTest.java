package JUnit;

import java.io.IOException;

import backend.DataAccess.DataAccessCSV;
import frontend.iController;
import org.junit.jupiter.api.Test;
import backend.DataAccess.iDataAccess;
import backend.Gamemaster;

public class SaveCSVTest {

    private static iController Spielleiter = new Gamemaster();


    @Test
    public void testLoad() throws IOException {
        iDataAccess dat = new DataAccessCSV();
        System.out.println(dat.load("test.csv"));
    }
 
    @Test
    public void testSave() throws IOException {

        Spielleiter.addPlayer("k",false);
        Spielleiter.addPlayer("f",false);
        Spielleiter.addPlayer("as",false);
        Spielleiter.addPlayer("asadf",false);
        Spielleiter.addPlayer("aqga",false);

        Spielleiter.startGame();

        iDataAccess dat = new DataAccessCSV();

        dat.save(Spielleiter.getRoundBuffer(), "test.csv");
    }
}
