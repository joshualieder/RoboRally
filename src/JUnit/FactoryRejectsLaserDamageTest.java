package JUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import backend.Gamemaster;
import backend.ProgrammCard;
import backend.RuleFactoryRejects;
import backend.defaultRulebook;
import frontend.iController;

public class FactoryRejectsLaserDamageTest {
	
	public static defaultRulebook rules = new RuleFactoryRejects();
    static Gamemaster Spielleiter = new Gamemaster(rules);
	@BeforeAll
    public static void setPositions() {
        //Position, bei der ein Move possible sein muesste; Field[4][6]: ist ein Pit?
        Spielleiter.addPlayer("Robot_1",false);
        Spielleiter.addPlayer("Robot_2",false);
        Spielleiter.addPlayer("Robot_3",false);
        Spielleiter.addPlayer("Robot_4",false);
        Spielleiter.addPlayer("Robot_5",false);
        Spielleiter.addPlayer("Robot_6",false);
        Spielleiter.startGame();

        for (int i = 0; i < Spielleiter.getPlayernameAndColor().length; i++) {
            System.out.print(Spielleiter.getPlayernameAndColor()[i] + " ");
        }
        System.out.println();
        //Spielleiter.move(1);
        String[][] MapPrint = Spielleiter.showMap();

        for (String[] strings : MapPrint) {
            for (String string : strings) {
                System.out.print(string);
            }
            System.out.println();
        }
        System.out.println();
    }

    @Test
    void test1() {
        //assertTrue(Spielleiter.move(1));
    }

    @Test
    void test2() {
    	Spielleiter.resetActivePlayer();
        Spielleiter.move(1);

        ProgrammCard card = ProgrammCard.rotateRight;

        Spielleiter.cardAction(card);
        Spielleiter.move(1);

        card = ProgrammCard.rotateRight;
        Spielleiter.cardAction(card);
        
        String[][] MapPrint = Spielleiter.showMap();

        for (String[] strings : MapPrint) {
            for (String string : strings) {
                System.out.print(string);
            }
            System.out.println();
        }
        System.out.println();

        System.out.println(Spielleiter.getActiveRobot().getPositionX() + " " + Spielleiter.getActiveRobot().getPositionY());

        System.out.println("Leben von Blue vor Schaden: " + Spielleiter.showRobolife());
        assertTrue(Spielleiter.damageLineofSightRobot());
        System.out.println("Leben von Blue nach Schaden: " + Spielleiter.showRobolife());


        
        card = ProgrammCard.rotateLeft;

        Spielleiter.cardAction(card);

        Spielleiter.move(1);


        
        card = ProgrammCard.rotateRight;
        Spielleiter.cardAction(card);

        //assertTrue(Spielleiter.damageLineofSightRobot());
        System.out.println("Schinken");
        MapPrint = Spielleiter.showMap();

        for (String[] strings : MapPrint) {
            for (String string : strings) {
                System.out.print(string);
            }
            System.out.println();
        }
        System.out.println();

        card = ProgrammCard.rotateLeft;
        Spielleiter.cardAction(card);
        Spielleiter.move(3);

        MapPrint = Spielleiter.showMap();

        for (String[] strings : MapPrint) {
            for (String string : strings) {
                System.out.print(string);
            }
            System.out.println();
        }
        System.out.println();


    }
}
