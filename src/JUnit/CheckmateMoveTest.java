package JUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;

import static org.junit.jupiter.api.Assertions.assertFalse;


import backend.Gamemaster;
import backend.ProgrammCard;
import backend.Robot;
import backend.RuleCheckmate;
import backend.Course;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;

public class CheckmateMoveTest {
	static Gamemaster Spielleiter = new Gamemaster();
    static Robot Spieler1 = new Robot("k");
    static Course course;
    static RuleCheckmate rules = new RuleCheckmate();
    //Diese Methode ist aus dem Backend kopiert 
    
    


    @BeforeAll
    public static void setPositions() {
    	//Position, bei der ein Move possible sein muesste; Field[4][6]: ist ein Pit?
    	Spielleiter.addPlayer("Penis",false);
    	Spielleiter.addPlayer("Penis",false);
    	Spielleiter.addPlayer("Penis",false);
    	Spielleiter.addPlayer("Penis",false);
    	Spielleiter.addPlayer("Penis",false);
    	Spielleiter.addPlayer("Penis",false);
    	Spielleiter.startGame();

    	for(int i = 0;i<Spielleiter.getPlayernameAndColor().length;i++) {
        	System.out.print(Spielleiter.getPlayernameAndColor()[i]+ " ");
        }
    	System.out.println();
    	//Spielleiter.move(1);
    	String[][] MapPrint = Spielleiter.showMap();
        
        for (String[] strings : MapPrint) {
            for (String string : strings) {
                System.out.print(string);
            }
            System.out.println();
        }
        System.out.println();
    }
    
    @Test
    public void moveTest() {
    	Spielleiter.move(1);
    }
    @AfterAll
    public static void mapPrint() {
    	Spielleiter.move(1);
    	String[][] MapPrint = Spielleiter.showMap();
        
        for (String[] strings : MapPrint) {
            for (String string : strings) {
                System.out.print(string);
            }
            System.out.println();
        }
        // check wheter West movement possible
        ProgrammCard card = ProgrammCard.rotateLeft;
        Spielleiter.cardAction(card);
        Spielleiter.move(1);
        MapPrint = Spielleiter.showMap();
        for (String[] strings : MapPrint) {
            for (String string : strings) {
                System.out.print(string);
            }
            System.out.println();
        }
        System.out.println();
        // check wheter North movement possible with wall
        card = ProgrammCard.rotateRight;
        Spielleiter.cardAction(card);
        Spielleiter.move(1);
        MapPrint = Spielleiter.showMap();
        for (String[] strings : MapPrint) {
            for (String string : strings) {
                System.out.print(string);
            }
            System.out.println();
        }
    }
}
