package JUnit;

import backend.Ringbuffer;
import backend.Robot;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RingbufferTest {

    static Ringbuffer buf = new Ringbuffer(4);
    static Robot[] testObjects = new Robot[4];


    @BeforeAll
    static void prepareTests() {
        testObjects[0] = new Robot("First Entry");
        testObjects[1] = new Robot("Second Entry");
        testObjects[2] = new Robot("Third Entry");
        testObjects[3] = new Robot("Fourth Entry");
    }

    //Testing first push
    @Test
    void test1() {
        assertTrue(buf.push(testObjects[0]));
    }

    //Testing filling
    @Test
    void test2() {
        boolean continuedSuccess = true;
        for (int i = 1; i < testObjects.length; i++) {
            continuedSuccess &= buf.push(testObjects[i]);
        }
        assertTrue(continuedSuccess);
    } 

//Verifiying Capacity Exception

    @Test
    void test3() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Ringbuffer temp = new Ringbuffer(0);
                });
    }

    //Verifying first peek
    @Test
    void test4() {
        assertEquals(testObjects[0], buf.peek());
    }

    //Verifying other peeks
    @Test
    void test5() {

        boolean continuedSuccess = true;
        for (int i = 1; i < buf.getCapacity(); i++) {
            continuedSuccess &= testObjects[i].equals(buf.peek());
        }

        assertTrue(continuedSuccess);

    }

    //Verifying overflow, return to first element

    @Test
    void test6() {
        assertEquals(testObjects[0], buf.peek());
    }
}
