package JUnit;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import Field.fullEffects;
import backend.Course;
import backend.DataAccess.DataAccessJSON;
import backend.DataAccess.iDataAccess;

public class JsonAccessTest {
	
	
	@Test
	public void testLoad() throws FileNotFoundException, IOException {
	iDataAccess dat = new DataAccessJSON();
	
	Course course = (Course) dat.load("Our_Map2.json");
	
	
	
	String[][] mapAsString = new String[course.getField().length][course.getField()[0].length];
	for (int i = 0; i < mapAsString.length; i++) {
		for (int j = 0; j < mapAsString[i].length; j++) {
			
				if (course.getField()[i][j].getFullEffects() == fullEffects.stand) {
					if (mapAsString[i][j] == null)
						mapAsString[i][j] = " # ";
				} else if (course.getField()[i][j].getFullEffects() == fullEffects.pit) {
					if (mapAsString[i][j] == null)
						mapAsString[i][j] = " x ";
				} else if (course.getField()[i][j].getFullEffects() == fullEffects.belt_north) {
					if (mapAsString[i][j] == null)
						mapAsString[i][j] = " ^ ";
				} else if (course.getField()[i][j].getFullEffects() == fullEffects.belt_south) {
					if (mapAsString[i][j] == null)
						mapAsString[i][j] = " v ";
				} else if (course.getField()[i][j].getFullEffects() == fullEffects.belt_east) {
					if (mapAsString[i][j] == null)
						mapAsString[i][j] = " > ";
				} else if (course.getField()[i][j].getFullEffects() == fullEffects.belt_west) {
					if (mapAsString[i][j] == null)
						mapAsString[i][j] = " < ";
				} else if (course.getField()[i][j].getFullEffects() == fullEffects.dock1
						|| course.getField()[i][j].getFullEffects() == fullEffects.dock2
						|| course.getField()[i][j].getFullEffects() == fullEffects.dock3
						|| course.getField()[i][j].getFullEffects() == fullEffects.dock4
						|| course.getField()[i][j].getFullEffects() == fullEffects.dock5
						|| course.getField()[i][j].getFullEffects() == fullEffects.dock6
						|| course.getField()[i][j].getFullEffects() == fullEffects.dock7
						|| course.getField()[i][j].getFullEffects() == fullEffects.dock8) {
					
						mapAsString[i][j] = " ~ ";
				} else if (course.getField()[i][j].getFullEffects() == fullEffects.flag1) {
					if (mapAsString[i][j] == null)
						mapAsString[i][j] = " 1 ";
				} else if (course.getField()[i][j].getFullEffects() == fullEffects.flag2) {
					if (mapAsString[i][j] == null)
						mapAsString[i][j] = " 2 ";
				} else if (course.getField()[i][j].getFullEffects() == fullEffects.flag3) {
					if (mapAsString[i][j] == null)
						mapAsString[i][j] = " 3 ";
				} else if (course.getField()[i][j].getFullEffects() == fullEffects.flag4) {
					if (mapAsString[i][j] == null)
						mapAsString[i][j] = " 4 ";
				} else if (course.getField()[i][j].getFullEffects() == fullEffects.repair) {
					if (mapAsString[i][j] == null)
						mapAsString[i][j] = " @ ";
				}
			}
		}


    for (String[] strings : mapAsString) {
        for (String string : strings) {
            System.out.print(string);
        }
        System.out.println();
    }
    System.out.println();

	
	}
	
	@Test
	public void testSave() throws FileNotFoundException, IOException{

		iDataAccess dat = new DataAccessJSON();
		Course course = new Course();
		dat.save(course, "Our_Map2.json");
	}
}
