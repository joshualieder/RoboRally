package JUnit;

import backend.Robot;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RobotTest {

    static Robot robot1, robot2, robot3, robot4;

    //For testing
    @BeforeAll
    public static void prepareRobots() {
        robot1 = new Robot("Heinrich");
        robot2 = new Robot("Horst-Guenther");
        robot3 = new Robot("Karl-Gustav");
        robot4 = new Robot("Augustus");


    }


    //Test Position Sets
    @Test
    public void test1() {
        robot1.setPositionX(1);
        robot1.setPositionY(1);
        assertTrue(robot1.getPositionX() == 1 && robot1.getPositionY() == 1);

    }

    //Test Power Switch
    @Test
    public void test2() {
        boolean initialOff = robot2.isPower();
        robot2.setPower(true);
        boolean thenOn = robot2.isPower();
        robot2.setPower(true);
        boolean andOffAgain = robot2.isPower();
        boolean result = !initialOff && thenOn && (!andOffAgain);
        assertEquals(new Boolean(result), Boolean.TRUE);
    }


}
