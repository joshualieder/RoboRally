package JUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import backend.Gamemaster;
import backend.Ringbuffer;
import backend.Robot;
import backend.RuleFactoryRejects;
import backend.defaultRulebook;
import frontend.iController;

public class FactoryRejectsDealCardTest {
	public static defaultRulebook rules = new RuleFactoryRejects();
    static iController Spielleiter = new Gamemaster(rules);
    
    static Ringbuffer Spieleranzahl = new Ringbuffer(5);
    static Robot Spieler1 = new Robot("k");
    static Robot Spieler2 = new Robot("j");
    static Robot Spieler3 = new Robot("f");
    static Robot Spieler4 = new Robot("f");

    @Test
    public void SpielerInBuffer() {
        Spieleranzahl.push(Spieler1);
        Spieleranzahl.push(Spieler2);
        Spieleranzahl.push(Spieler2);
    }

    @Test
    public void SpielerInSpielleiter() {
        Spielleiter.addPlayer("k",false);
        Spielleiter.addPlayer("f",false);
        Spielleiter.addPlayer("as",false);
        Spielleiter.addPlayer("asadf",false);
        Spielleiter.addPlayer("aqga",false);
        Spielleiter.addPlayer("aqwe",false);
    }

    @Test
    public void PlayerToRoundBuffer() {
        assertEquals(Spielleiter.startGame(), "");
    }

    @Test
    public void showMap() {
        assertEquals(Spielleiter.showMap(), "");
    }
    
}
