package JUnit;

import backend.Gamemaster;
import backend.Robot;
import backend.RuleFactoryRejects;
import backend.defaultRulebook;
import frontend.iController;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FactoryRejectsStartlifeTest {
    static Robot robot1, robot2, robot3, robot4;

	public static defaultRulebook rules = new RuleFactoryRejects();
    static iController playRoboRally = new Gamemaster(rules);
    static iController playRoboRally1 = new Gamemaster();
    //For testing
    @BeforeAll
    public static void prepareRobots() {
        
        playRoboRally.addPlayer("asdf", false);
        playRoboRally.addPlayer("asd", false);
        playRoboRally.addPlayer("sdfadf", false);
        playRoboRally.addPlayer("sdafdsf", false);
        playRoboRally.addPlayer("asdfasdfasddf", false);
        
        playRoboRally1.addPlayer("asdf", false);
        playRoboRally1.addPlayer("asd", false);
        playRoboRally1.addPlayer("sdfadf", false);
        playRoboRally1.addPlayer("sdafdsf", false);
        playRoboRally1.addPlayer("asdfasdfasddf", false);
        
    }


    //Test Position Sets
    @Test
    public void test1() {
    	playRoboRally.startGame();
    	assertEquals(playRoboRally.showRobolife(), 8);
    	System.out.println(playRoboRally.showRobolife());
    }
    
    @Test 
    public void test2() {
    	playRoboRally1.startGame();
    	assertTrue(playRoboRally1.powerDown());
    
    	assertFalse(playRoboRally.powerDown());
    }
    
   

}
