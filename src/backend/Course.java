package backend;

import Field.Field;
import Field.fullEffects;
import Field.sideEffects;

import java.util.ArrayList;

public class Course {

    private static final Field stand = new Field(null, null, null, null, fullEffects.stand);
    private static final Field stand_northwall = new Field(sideEffects.wall, null, null, null, fullEffects.stand);
    private static final Field stand_eastwall = new Field(null, sideEffects.wall, null, null, fullEffects.stand);
    private static final Field stand_southwall = new Field(null, null, sideEffects.wall, null, fullEffects.stand);
    private static final Field stand_westwall = new Field(null, null, null, sideEffects.wall, fullEffects.stand);
    private static final Field repair = new Field(null, null, null, null, fullEffects.repair);
    private static final Field pit = new Field(null, null, null, null, fullEffects.pit);

    private static final Field flag1 = new Field(null, null, null, null, fullEffects.flag1);
    private static final Field flag2 = new Field(null, null, null, null, fullEffects.flag2);
    private static final Field flag3 = new Field(null, null, null, null, fullEffects.flag3);
    private static final Field flag4 = new Field(null, null, null, null, fullEffects.flag4);

    private static final Field dock1_westwall = new Field(null, null, null, sideEffects.wall, fullEffects.dock1); // docks only for this field right now!
    private static final Field dock2_westwall = new Field(null, null, null, sideEffects.wall, fullEffects.dock2);
    private static final Field dock3_westwall = new Field(null, null, null, sideEffects.wall, fullEffects.dock3);
    private static final Field dock4 = new Field(null, null, null, null, fullEffects.dock4);
    private static final Field dock5_westwall = new Field(null, null, null, sideEffects.wall, fullEffects.dock5);
    private static final Field dock6 = new Field(null, null, null, null, fullEffects.dock6);
    private static final Field dock7 = new Field(null, null, null, null, fullEffects.dock7);
    private static final Field dock8_westwall = new Field(null, null, null, sideEffects.wall, fullEffects.dock8);

    private static final Field northbelt = new Field(null, null, null, null, fullEffects.belt_north);
    private static final Field eastbelt = new Field(null, null, null, null, fullEffects.belt_east);
    private static final Field southbelt = new Field(null, null, null, null, fullEffects.belt_south);
    private static final Field westbelt = new Field(null, null, null, null, fullEffects.belt_west);

    private static final Field northbelt_northwall = new Field(sideEffects.wall, null, null, null, fullEffects.belt_north);
    private static final Field eastbelt_northwall = new Field(sideEffects.wall, null, null, null, fullEffects.belt_east);
    private static final Field southbelt_northwall = new Field(sideEffects.wall, null, null, null, fullEffects.belt_south);
    private static final Field westbelt_northwall = new Field(sideEffects.wall, null, null, null, fullEffects.belt_west);

    private static final Field northbelt_eastwall = new Field(null, sideEffects.wall, null, null, fullEffects.belt_north);
    private static final Field eastbelt_eastwall = new Field(null, sideEffects.wall, null, null, fullEffects.belt_east);
    private static final Field southbelt_eastwall = new Field(null, sideEffects.wall, null, null, fullEffects.belt_south);
    private static final Field westbelt_eastwall = new Field(null, sideEffects.wall, null, null, fullEffects.belt_west);

    private static final Field northbelt_southwall = new Field(null, null, sideEffects.wall, null, fullEffects.belt_north);
    private static final Field eastbelt_southwall = new Field(null, null, sideEffects.wall, null, fullEffects.belt_east);
    private static final Field southbelt_southwall = new Field(null, null, sideEffects.wall, null, fullEffects.belt_south);
    private static final Field westbelt_southwall = new Field(null, null, sideEffects.wall, null, fullEffects.belt_west);

    private static final Field northbelt_westwall = new Field(null, null, null, sideEffects.wall, fullEffects.belt_north);
    private static final Field eastbelt_westwall = new Field(null, null, null, sideEffects.wall, fullEffects.belt_east);
    private static final Field southbelt_westwall = new Field(null, null, null, sideEffects.wall, fullEffects.belt_south);
    private static final Field westbelt_westwall = new Field(null, null, null, sideEffects.wall, fullEffects.belt_west);
    private Field[][] field;

    protected ArrayList<int[]> laserList = new ArrayList<int[]>();

    /**
     * Feld als zweidimensionales Array.
     *
     * @param temp (ist ein zweidimensionales Array, welches UNSERE Map beinhaltet)
     */
    public Course() {
        Field[][] temp = {
                // 00
                {pit, pit, pit, pit, pit, pit, pit, pit, pit, pit, pit, pit, pit, pit},
                // 10
                {pit, stand, stand, stand_northwall, stand, stand_northwall, stand, stand, stand_northwall, stand, stand_northwall, stand, repair, pit},
                // 20
                {pit, stand, eastbelt, eastbelt, eastbelt_southwall, eastbelt, eastbelt_southwall, eastbelt_southwall, eastbelt, eastbelt_southwall, eastbelt, southbelt, stand, pit},
                {pit, stand_westwall, northbelt, eastbelt, stand, eastbelt, stand, westbelt, flag1, westbelt, stand, southbelt, stand_eastwall, pit},
                {pit, stand, northbelt_eastwall, stand, pit, stand, eastbelt, stand, westbelt, stand, westbelt, southbelt_westwall, stand, pit},
                {pit, stand_westwall, northbelt, eastbelt, stand, eastbelt, stand, pit, stand, westbelt, stand, southbelt, stand_eastwall, pit},
                {pit, stand, northbelt_eastwall, stand, eastbelt, stand, repair, stand, westbelt, stand, westbelt, southbelt_westwall, stand, pit},
                {pit, stand, northbelt_eastwall, eastbelt, stand, eastbelt, stand, repair, stand, pit, stand, southbelt_westwall, stand, pit},
                {pit, stand_westwall, northbelt, stand, eastbelt, stand, pit, stand, westbelt, stand, westbelt, southbelt, stand_eastwall, pit},
                {pit, stand, northbelt_eastwall, eastbelt, flag2, eastbelt, stand, westbelt, stand, westbelt, stand, southbelt_westwall, stand, pit},
                {pit, stand_westwall, northbelt, stand, eastbelt, stand, eastbelt, stand, westbelt, stand, westbelt, southbelt, stand_eastwall, pit},
                {pit, stand, northbelt, westbelt, westbelt_northwall, westbelt, westbelt_northwall, westbelt_northwall, westbelt, westbelt_northwall, westbelt, westbelt, stand, pit},
                {pit, repair, stand, stand_southwall, stand, stand_southwall, stand, stand, stand_southwall, stand, stand_southwall, stand, stand, pit},
                {pit, stand, stand, stand_northwall, stand, stand_northwall, stand, stand, stand_northwall, stand, stand_northwall, stand, stand, pit},
                {pit, stand, stand, stand, stand, stand, stand, stand, stand, stand, stand, stand, stand, pit},
                {pit, dock7, dock5_westwall, stand, dock3_westwall, stand, dock1_westwall, dock2_westwall, stand_westwall, dock4, stand_westwall, dock6, dock8_westwall, pit},
                {pit, stand, stand, stand_southwall, stand, stand_southwall, stand, stand, stand_southwall, stand, stand_southwall, stand, stand, pit},
                {pit, pit, pit, pit, pit, pit, pit, pit, pit, pit, pit, pit, pit, pit}

        };

        field = temp;
    }

    /**
     * Wenn ein Feld vorinitialisiert ist
     *
     * @param tmp
     */
    public Course(Field[][] tmp) {
        field = tmp;
    }

    /**
     * Gibt das gesammte zweidimensionale FeldArray zurÃ¼ck
     *
     * @return
     */
    public Field[][] getField() {
        return field;
    }

    /**
     * Gibt uns einzelne Spielfelder zurÃ¼ck
     *
     * @param x
     * @param y
     * @return
     */
    public Field getSingleField(int y, int x) {
        return field[y][x];
    }

    /**
     * Getter für die identifizierten Laser auf der Map
     *
     * @return ArrayList<int   [   ]       =       {   yAchse   ,       xAchse   ,       direction   }>
     */

    public ArrayList<int[]> getLaserList() {
        return laserList;
    }

    /**
     * Methode, um das aktuelle Spielfeld zu durchlaufen und die Laser
     * mit Position x, y und Richtung zu identifizieren. Diese werden in einem
     * int[] gespeichert und in eine die ArrayList laserList hinzugefügt.
     */

    public void LaserList() {
        for (int yAchse = 0; yAchse < field.length; yAchse++) {
            for (int xAchse = 0; xAchse < field[0].length; xAchse++) {
                Field tmpFieldLaserCheck = getSingleField(yAchse, xAchse);
                for (int k = 0; k < 4; k++) {
                    if (tmpFieldLaserCheck.getSideEffects()[k] == sideEffects.laser) {
                        int direction = k;
                        int[] laser = {yAchse, xAchse, direction};
                        laserList.add(laser);

                    }
                }


            }
        }
    }

}