package backend;

import Field.fullEffects;

import java.io.Serializable;

public class Robot implements Serializable {

	private String name;
	private String color;
	private int[] hand = new int[9];
	private int[] registry = new int[5];
	private int robolife = 10;
	private int playerlife = 3;
	private int positionX;
	private int positionY;
	private boolean power = true;
	private char viewDirection = 'N';
	private int[] flagNumber;
	private fullEffects spawnLocation;

	private boolean registryReady = false;
	boolean ki = false;

	public boolean isKi() {
		return ki;
	}

	public void setKi(boolean ki) {
		this.ki = ki;
	}

	/**
	 * Construktor fuer den Robot.
	 *
	 * @param name,
	 */
	public Robot(String name) {
		setName(name);
	}

	/**
	 * Die Positionen vom Roboter werden auf die Respawn-Position aktualisiert
	 *
	 * @param positionX
	 * @param positionY
	 */
	public void respawn(int positionX, int positionY) {
		setPositionX(positionX);
		setPositionY(positionY);
	}

	/**
	 * Setzt je nach Richtung die ViewDirection neu.
	 *
	 * @param turndirection
	 */
	public void cardTurn(String turndirection) {
		switch (turndirection) {
		case ("left"):
			switch (this.getViewDirection()) {
			case ('N'):
				this.setViewDirection('W');
				break;
			case ('E'):
				this.setViewDirection('N');
				break;
			case ('S'):
				this.setViewDirection('E');
				break;
			case ('W'):
				this.setViewDirection('S');
				break;
			}
			break;
		case ("right"):
			switch (this.getViewDirection()) {
			case ('N'):
				this.setViewDirection('E');
				break;
			case ('E'):
				this.setViewDirection('S');
				break;
			case ('S'):
				this.setViewDirection('W');
				break;
			case ('W'):
				this.setViewDirection('N');
				break;
			}
			break;
		case ("uturn"):
			switch (this.getViewDirection()) {
			case ('N'):
				this.setViewDirection('S');
				break;
			case ('E'):
				this.setViewDirection('W');
				break;
			case ('S'):
				this.setViewDirection('N');
				break;
			case ('W'):
				this.setViewDirection('E');
				break;
			}
			break;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int[] getHand() {
		return hand;
	}

	/**
	 * Erstellt eine Kopie der vom Gamemaster generierten Hand f�r den Spieler
	 *
	 * @param generatedSetHand
	 */
	public void setHand(int[] generatedSetHand) {
		hand = generatedSetHand;
	}

	public void setHandLength(int amountCards) {
		hand = new int[amountCards];
	}

	public int[] getRegistry() {
		return registry;
	}

	public void setRegistry(int[] filledRegistry) {

		this.registry = filledRegistry;
	}

	public int getRobolife() {
		return robolife;
	}

	public void setRobolife(int robolife) {
		if (robolife >= 10)
			this.robolife = 10;
		else {
			this.robolife = robolife;
		}
	}

	public int getPlayerlife() {
		return playerlife;
	}

	public void setPlayerlife(int playerlife) {
		this.playerlife = playerlife;
	}

	public boolean isPower() {
		return power;
	}

	public void setPower(boolean power) {
		if (this.power == true) {
			this.power = false;
		} else {
			this.power = true;
		}
	}

	public char getViewDirection() {
		return viewDirection;
	}

	public void setViewDirection(char viewDirection) {
		this.viewDirection = viewDirection;
	}

	public int[] getFlagNumber() {
		return flagNumber;
	}

	/**
	 * Es wird durch das aktuelle int[] iteriert und der erste Wert == 0 auf 1
	 * gesetzt.
	 *
	 * @param flagNumber
	 */
	public void captureFlag() {

		for (int i = 0; i < flagNumber.length; i++) {
			if (flagNumber[i] == 0) {
				flagNumber[i] = 1;
				break;
			}
		}

	}

	public void setFlagNumber(int[] flagNumberArray) {
		this.flagNumber = flagNumberArray;
	}

	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}

	public fullEffects getSpawnLocation() {
		return spawnLocation;
	}

	public void setSpawnLocation(fullEffects spawnLocation) {
		this.spawnLocation = spawnLocation;
	}

	public boolean getPower() {
		return power;
	}

	public void setRegistryReady(boolean value) {
		if (value == false) {
			int[] clearRegistry = new int[registry.length];
			for (int i = 0; i < registry.length; i++) {
				clearRegistry[i] = 0;
			}
			setRegistry(clearRegistry);
		}
		registryReady = value;
	}

	public boolean getRegistryReady() {
		return registryReady;
	}

	/**
	 * Iteriert durch das Registry des Spielers. Sofern noch kein Wert hinterlegt
	 * ist, wird Priority-Number im Register[] abgelegt. Methode kann dann erneut
	 * aufgerufen werden, bis 4 mal geschehen. Achtung! IndexOutOfBoundException!
	 * Aufruf mehr als 4 mal muss register wieder komplett auf {0,0,0,0} setzen!
	 *
	 * @param index
	 *            = Indexstelle, welche Karte ausgewaehlt wurde.
	 * @return boolean
	 */
	public boolean chooseCard(int index) {

		int[] registerFill = this.getRegistry();

		for (int i = 0; i < this.getRegistry().length; i++) {
			if (this.getRegistry()[i] == 0) {
				registerFill[i] = this.getHand()[index]; // @ToDo Fehler beim Spielablauf:
															// "ArrayIndexOutOfBoundsException: 8"
				this.setRegistry(registerFill);
				return true;
			}

		}

		return false;
	}
}
