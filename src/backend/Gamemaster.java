package backend;

import Field.Field;
import Field.fullEffects;
import frontend.iController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

public class Gamemaster implements iController {
    private Robot activePlayer;
    private Ringbuffer Playerbuffer = new Ringbuffer(8);
    private Ringbuffer RoundBuffer;
    private Course course = new Course();
    private defaultRulebook rules = new RuleCheckmate();
    private int playerCount = 0;

    private HashMap<Integer, ProgrammCard> cardMap = new HashMap<Integer, ProgrammCard>();

    public Gamemaster() {

    }

    public Gamemaster(defaultRulebook rulebook) {
        rules = rulebook;
    }

    /**
     * Alle Karten mit Priority number. CardMap cleared Alle Karten werden mit
     * Prioritynumbers eingetragen.
     */
    public void deck() {
        cardMap.clear();
        cardMap.put(10, ProgrammCard.UTurn);
        cardMap.put(20, ProgrammCard.UTurn);
        cardMap.put(30, ProgrammCard.UTurn);
        cardMap.put(40, ProgrammCard.UTurn);
        cardMap.put(50, ProgrammCard.UTurn);
        cardMap.put(60, ProgrammCard.UTurn);

        cardMap.put(70, ProgrammCard.rotateLeft);
        cardMap.put(80, ProgrammCard.rotateRight);
        cardMap.put(90, ProgrammCard.rotateLeft);
        cardMap.put(100, ProgrammCard.rotateRight);
        cardMap.put(110, ProgrammCard.rotateLeft);
        cardMap.put(120, ProgrammCard.rotateRight);
        cardMap.put(130, ProgrammCard.rotateLeft);
        cardMap.put(140, ProgrammCard.rotateRight);
        cardMap.put(150, ProgrammCard.rotateLeft);
        cardMap.put(160, ProgrammCard.rotateRight);
        cardMap.put(170, ProgrammCard.rotateLeft);
        cardMap.put(180, ProgrammCard.rotateRight);
        cardMap.put(190, ProgrammCard.rotateLeft);
        cardMap.put(200, ProgrammCard.rotateRight);
        cardMap.put(210, ProgrammCard.rotateLeft);
        cardMap.put(220, ProgrammCard.rotateRight);
        cardMap.put(230, ProgrammCard.rotateLeft);
        cardMap.put(240, ProgrammCard.rotateRight);
        cardMap.put(250, ProgrammCard.rotateLeft);
        cardMap.put(260, ProgrammCard.rotateRight);
        cardMap.put(270, ProgrammCard.rotateLeft);
        cardMap.put(280, ProgrammCard.rotateRight);
        cardMap.put(290, ProgrammCard.rotateLeft);
        cardMap.put(300, ProgrammCard.rotateRight);
        cardMap.put(310, ProgrammCard.rotateLeft);
        cardMap.put(320, ProgrammCard.rotateRight);
        cardMap.put(330, ProgrammCard.rotateLeft);
        cardMap.put(340, ProgrammCard.rotateRight);
        cardMap.put(350, ProgrammCard.rotateLeft);
        cardMap.put(360, ProgrammCard.rotateRight);
        cardMap.put(370, ProgrammCard.rotateLeft);
        cardMap.put(380, ProgrammCard.rotateRight);
        cardMap.put(390, ProgrammCard.rotateLeft);
        cardMap.put(400, ProgrammCard.rotateRight);
        cardMap.put(410, ProgrammCard.rotateLeft);
        cardMap.put(420, ProgrammCard.rotateRight);

        cardMap.put(430, ProgrammCard.backup);
        cardMap.put(440, ProgrammCard.backup);
        cardMap.put(450, ProgrammCard.backup);
        cardMap.put(460, ProgrammCard.backup);
        cardMap.put(470, ProgrammCard.backup);
        cardMap.put(480, ProgrammCard.backup);

        cardMap.put(490, ProgrammCard.move1);// ist da
        cardMap.put(500, ProgrammCard.move1);
        cardMap.put(510, ProgrammCard.move1);
        cardMap.put(520, ProgrammCard.move1);
        cardMap.put(530, ProgrammCard.move1);
        cardMap.put(540, ProgrammCard.move1);
        cardMap.put(550, ProgrammCard.move1);
        cardMap.put(560, ProgrammCard.move1);
        cardMap.put(570, ProgrammCard.move1);
        cardMap.put(580, ProgrammCard.move1);
        cardMap.put(590, ProgrammCard.move1);
        cardMap.put(600, ProgrammCard.move1);
        cardMap.put(610, ProgrammCard.move1);
        cardMap.put(620, ProgrammCard.move1);
        cardMap.put(630, ProgrammCard.move1);
        cardMap.put(640, ProgrammCard.move1);
        cardMap.put(650, ProgrammCard.move1);
        cardMap.put(660, ProgrammCard.move1);// ist da

        cardMap.put(670, ProgrammCard.move2);
        cardMap.put(680, ProgrammCard.move2);
        cardMap.put(690, ProgrammCard.move2);
        cardMap.put(700, ProgrammCard.move2);
        cardMap.put(710, ProgrammCard.move2);
        cardMap.put(720, ProgrammCard.move2);
        cardMap.put(730, ProgrammCard.move2);
        cardMap.put(740, ProgrammCard.move2);
        cardMap.put(750, ProgrammCard.move2);
        cardMap.put(760, ProgrammCard.move2);
        cardMap.put(770, ProgrammCard.move2);
        cardMap.put(780, ProgrammCard.move2);

        cardMap.put(790, ProgrammCard.move3);
        cardMap.put(800, ProgrammCard.move3);
        cardMap.put(810, ProgrammCard.move3);
        cardMap.put(820, ProgrammCard.move3);
        cardMap.put(830, ProgrammCard.move3);
        cardMap.put(840, ProgrammCard.move3);
    }

    /**
     * Karten werden ausgeteilt. Methode rekursiv.
     *
     * @param @todo
     * @return boolean true = an jeden Spieler wurden Karten ausgeteilt false = ein
     * Fehler im RingBuffer ist aufgetaucht? << Exception werfen?
     */
    public boolean dealCardsNewRound() {

        deck(); // Das Deck wird mit der Methode erneut bef�llt
        // Abfrage, ob Bufferstelle != null und der Spieler keine Karten auf der Hand
        // hat
        RoundBuffer.resetRead();
        for (int z = 0; z < RoundBuffer.getCapacity(); z++) {
            activePlayer = RoundBuffer.peek();
            this.lockRobolifeHandRegistry(activePlayer);
            while (activePlayer.getHand()[activePlayer.getHand().length -1] == 0) {
                Random hashKey = new Random();
                int randomHashKey = 0;
                int[] generatedSetHand = activePlayer.getHand();
                for (int i = 0; i < activePlayer.getHand().length; i++) {
                    randomHashKey = (hashKey.nextInt(83) + 1) * 10;
                    while (!cardMap.containsKey(randomHashKey)) {
                        randomHashKey = (hashKey.nextInt(83) + 1) * 10;
                    }
                    generatedSetHand[i] = randomHashKey;
                    cardMap.remove(randomHashKey);
                }
                activePlayer.setHand(generatedSetHand);
            }
        }
        return true;
    }

    /**
     * holt aktuellen Spieler am RoundBuffer-Z�hler in die ActivePlayer Variable
     * und erhöt dabei den Readpointer in Roundbuffer.
     */
    public void setsActivePlayer() {
        Robot robot = activePlayer;
        while (robot.equals(activePlayer)) {
            activePlayer = RoundBuffer.peek();

        }

    }

    /**
     * Platziert die Roboter auf dem Spielfeld. ABER: Spieler 1 nicht auf Dock1
     * sondern erstes gefundene Dock
     */
    public void placeRobots() {

        Field[][] fields = course.getField();

        int[] dockingY = new int[8];
        int[] dockingX = new int[8];
        // Sucht die Docks in der aktuellen Map
        for (int yAchse = 0; yAchse < fields.length; yAchse++) {
            for (int xAchse = 0; xAchse < fields[yAchse].length; xAchse++) {

                fullEffects curr = fields[yAchse][xAchse].getFullEffects();
                if (curr == fullEffects.dock1 || curr == fullEffects.dock2 || curr == fullEffects.dock3
                        || curr == fullEffects.dock4 || curr == fullEffects.dock5 || curr == fullEffects.dock6
                        || curr == fullEffects.dock7 || curr == fullEffects.dock8) {
                    switch (curr) {
                        case dock1:
                            dockingY[0] = yAchse;
                            dockingX[0] = xAchse;
                            break;
                        case dock2:
                            dockingY[1] = yAchse;
                            dockingX[1] = xAchse;
                            break;
                        case dock3:
                            dockingY[2] = yAchse;
                            dockingX[2] = xAchse;
                            break;
                        case dock4:
                            dockingY[3] = yAchse;
                            dockingX[3] = xAchse;
                            break;
                        case dock5:
                            dockingY[4] = yAchse;
                            dockingX[4] = xAchse;
                            break;
                        case dock6:
                            dockingY[5] = yAchse;
                            dockingX[5] = xAchse;
                            break;
                        case dock7:
                            dockingY[6] = yAchse;
                            dockingX[6] = xAchse;
                            break;
                        case dock8:
                            dockingY[7] = yAchse;
                            dockingX[7] = xAchse;
                            break;

                    }

                }
            }

        }

        // Setzt die Werte des Docks in die jeweiligen Spieler
        for (int i = 0; i < RoundBuffer.getCapacity(); i++) {
            Robot player = RoundBuffer.peek();
            if (player != null) {
                switch (player.getColor()) {
                    case "Pink":
                        player.setSpawnLocation(course.getSingleField(dockingY[0], dockingX[0]).getFullEffects());
                        player.setPositionX(dockingX[0]);
                        player.setPositionY(dockingY[0]);
                        break;
                    case "Blue":
                        player.setSpawnLocation(course.getSingleField(dockingY[1], dockingX[1]).getFullEffects());
                        player.setPositionX(dockingX[1]);
                        player.setPositionY(dockingY[1]);
                        break;
                    case "Green":
                        player.setSpawnLocation(course.getSingleField(dockingY[2], dockingX[2]).getFullEffects());
                        player.setPositionX(dockingX[2]);
                        player.setPositionY(dockingY[2]);
                        break;
                    case "Orange":
                        player.setSpawnLocation(course.getSingleField(dockingY[3], dockingX[3]).getFullEffects());
                        player.setPositionX(dockingX[3]);
                        player.setPositionY(dockingY[3]);
                        break;
                    case "White":
                        player.setSpawnLocation(course.getSingleField(dockingY[4], dockingX[4]).getFullEffects());
                        player.setPositionX(dockingX[4]);
                        player.setPositionY(dockingY[4]);
                        break;
                    case "Cyan":
                        player.setSpawnLocation(course.getSingleField(dockingY[5], dockingX[5]).getFullEffects());
                        player.setPositionX(dockingX[5]);
                        player.setPositionY(dockingY[5]);
                        break;
                    case "Yellow":
                        player.setSpawnLocation(course.getSingleField(dockingY[6], dockingX[6]).getFullEffects());
                        player.setPositionX(dockingX[6]);
                        player.setPositionY(dockingY[6]);
                        break;
                    case "Red":
                        player.setSpawnLocation(course.getSingleField(dockingY[7], dockingX[7]).getFullEffects());
                        player.setPositionX(dockingX[7]);
                        player.setPositionY(dockingY[7]);
                        break;
                    // @TODO Hinzuf�gen Fehlermeldung bei >8 Spieler
                    default:
                        break;
                }

            }
        }
    }

    /**
     * Schaut sich an, welche Karte im Register liegt und ruft die cardAction() zur
     * Durchf�hrung auf. Nachdem eine Registerstelle abgearbeitet wurde, wird die
     * fieldAction(), LaserDamage sowie checkFlag aufgerufen.
     */
    public void playRound() {
        for (int i = 0; i < 5; i++) {
            sortRound(i);
            RoundBuffer.resetRead();
            activePlayer = RoundBuffer.peek();

            System.out.println("schinken");
            for (int j = 0; j < RoundBuffer.getCapacity(); j++) {
                System.out.println(activePlayer.getClass());
                System.out.println(activePlayer.getColor() + "Position[" + activePlayer.getPositionX() + "]["
                        + activePlayer.getPositionY() + "]" + activePlayer.getViewDirection());
                if (activePlayer.getPower() == true) {
                    // System.out.println(activePlayer.getRegistry()[i]);
                	if(!(i >= activePlayer.getRegistry().length)) {
                    switch (activePlayer.getRegistry()[i]) {
                        case (10):// Fallthrough UTurn
                        case (20):
                        case (30):
                        case (40):
                        case (50):
                        case (60):
                            cardAction(ProgrammCard.UTurn);
                            break;
                        case (70):// Fallthrough RotateLeft
                        case (90):
                        case (110):
                        case (130):
                        case (150):
                        case (170):
                        case (190):
                        case (210):
                        case (230):
                        case (250):
                        case (270):
                        case (290):
                        case (310):
                        case (330):
                        case (350):
                        case (370):
                        case (390):
                        case (410):
                            cardAction(ProgrammCard.rotateLeft);
                            break;
                        case (80):// Fallthrough RotateRight
                        case (100):
                        case (120):
                        case (140):
                        case (160):
                        case (180):
                        case (200):
                        case (220):
                        case (240):
                        case (260):
                        case (280):
                        case (300):
                        case (320):
                        case (340):
                        case (360):
                        case (380):
                        case (400):
                        case (420):
                            cardAction(ProgrammCard.rotateRight);
                            break;
                        case (430):// Fallthrough backup
                        case (440):
                        case (450):
                        case (460):
                        case (470):
                        case (480):
                            cardAction(ProgrammCard.backup);
                            break;
                        case (490): // Fallthrough move1
                        case (500):
                        case (510):
                        case (520):
                        case (530):
                        case (540):
                        case (550):
                        case (560):
                        case (570):
                        case (580):
                        case (590):
                        case (600):
                        case (610):
                        case (620):
                        case (630):
                        case (640):
                        case (650):
                        case (660):
                            cardAction(ProgrammCard.move1);
                            break;
                        case (670):// Fallthrough move2
                        case (680):
                        case (690):
                        case (700):
                        case (710):
                        case (720):
                        case (730):
                        case (740):
                        case (750):
                        case (760):
                        case (770):
                        case (780):
                            cardAction(ProgrammCard.move2);
                            break;
                        case (790):// Fallthrough move3
                        case (800):
                        case (810):
                        case (820):
                        case (830):
                        case (840):
                            cardAction(ProgrammCard.move3);
                            break;
                    }
                	}
                    activePlayer = RoundBuffer.peek();
                }

            }

            fieldAction();

            if (damageLineofSightRobot() == true) {
                System.out.println("Schaden verursacht!");
            }
            int position_y;
            int position_x;
            int direction;

            if (course.getLaserList() != null) {
                for (int[] laser : course.getLaserList()) {
                    position_y = laser[0];
                    position_x = laser[1];
                    direction = laser[2];
                    if (damageLineofSightLaser(position_x, position_y, direction) == true) {
                        System.out.println("Laserdamage!");
                    } else {
                        System.out.println("Keiner wurde von einem Feld-Laser getroffen");
                    }
                }
            }

            respawnRobots();
        }
        for (int i = 0; i < RoundBuffer.getCapacity(); i++) {
            Robot robot = RoundBuffer.peek();
            lockRobolifeHandRegistry(robot);
            robot.setRegistryReady(false);
            System.out.print(robot.getRobolife() + robot.getColor() + "  ");
        }
        dealCardsNewRound();
        System.out.println("____________________________________");

    }

    /**
     * setzt ActivPlayer auf übergebenen Roboter
     *
     * @param robot
     */
    public void setRoundPlayer(Robot robot) {
        activePlayer = robot;
    }

    /**
     * should sort the Roundbuffer to the right order via the index of the hand
     * array
     *
     * @param index
     */

    public void sortRound(int index) {
        Ringbuffer tmp = new Ringbuffer(RoundBuffer.getCapacity());
        Robot robot = null;

        RoundBuffer.resetRead();
        int[] valuesToSort = new int[tmp.getCapacity()];
        int[] valuesSorted = new int[tmp.getCapacity()];

        for (int i = 0; i < RoundBuffer.getCapacity(); i++) {
            robot = RoundBuffer.peek();
            if (index >= robot.getRegistry().length) {
                valuesToSort[i] = 0;
            } else {
                valuesToSort[i] = robot.getRegistry()[index];
            }
            // eeehm. Wenn Robolife < 5 , dann ExceptionThrow
        }

        int[] tmpPrio = new int[tmp.getCapacity()];
        for (int i = 0; i < valuesSorted.length; i++) {
            tmpPrio[i] = valuesToSort[i];
        }
        Arrays.sort(tmpPrio);

        for (int i = tmpPrio.length; i > 0; i--) {
            valuesSorted[tmpPrio.length - i] = tmpPrio[i - 1];
        }

        for (int i = 0; i < valuesSorted.length; i++) {
            System.out.println(valuesSorted[i]);
        }
        Robot temp;
        for (int i = 0; i < valuesSorted.length; i++) {
            for (int j = 0; j < valuesToSort.length; j++) {

                if (valuesSorted[i] == valuesToSort[j]) {
                    RoundBuffer.setReadPointer(j);
                    temp = RoundBuffer.peek();
                    System.out.println(temp.getColor());
                    tmp.push(temp);

                }
            }
        }
        System.out.println();
        tmp.resetRead();
        RoundBuffer = tmp;
    }


    /**
     * Gibt Register von aktiven Spieler zurück.
     */
    public int[] showRegistry() {
        return activePlayer.getRegistry();
    }

    /**
     * Ruft move Methoden auf oder ruft CardTurn für Robots auf.
     *
     * @param card
     */
    public void cardAction(ProgrammCard card) {
        System.out.println("cardAction entered"); // @Todo
        switch (card) {
            case move1:
                move(1);
                System.out.println("Move1 ausgef�hrt");
                break;
            case move2:
                move(2);
                System.out.println("Move2 ausgef�hrt");
                break;
            case move3:
                move(3);
                System.out.println("Move3 ausgef�hrt");
                break;
            case rotateRight:
                activePlayer.cardTurn("right");
                System.out.println("rotateRight ausgef�hrt");
                break;
            case rotateLeft:
                activePlayer.cardTurn("left");
                System.out.println("rotateLeft ausgef�hrt");
                break;
            case UTurn:
                activePlayer.cardTurn("uturn");
                System.out.println("uTurn ausgef�hrt");
                break;
        }
        System.out.println("cardAction done"); // @Todo
        // check Roundbuffer)

        // ganzes Register durchgucken
        // wenn da was 0 ist, weiter iterieren
        // wenn da keine null ist, dann passenden Methodenaufruf machen
    }

    /**
     * Respawned alle Robots auf der SpawnLocaton. Aufruf in playRound();
     */
    public void respawnRobots() {
        Robot robot;
        for (int i = 0; i < RoundBuffer.getCapacity(); i++) {
            robot = RoundBuffer.peek();
            if (robot.getRobolife() <= 0) {
                fullEffects spawn = robot.getSpawnLocation();

                // Was passiert wenn der zu respawnende Robotor auf einem Feld
                // landet auf das
                // ein anderer steht?
                // Methode um durch das Feld zu iterieren und nach der Spawn
                // location zu suchen:
                for (int yAchse = 0; yAchse < course.getField().length; yAchse++) {
                    for (int xAchse = 0; xAchse < course.getField()[yAchse].length; xAchse++) { // !! [0]?
                        if (course.getField()[yAchse][xAchse].getFullEffects() == spawn) {
                            robot.respawn(yAchse, xAchse);
                            return;
                        }
                    }
                }
                robot.setPlayerlife(robot.getPlayerlife() - 1);
                robot.setRobolife(10);
            }
        }
    }

    /**
     * Bewegt den Roboter, nachdem überprüft wird, ob dies möglich ist.
     *
     * @param steps
     * @return boolean
     */
    public boolean move(int steps) {
        System.out.println("move entered");
        if (activePlayer.getPower() == true) {
            int position_x = activePlayer.getPositionX();
            int position_y = activePlayer.getPositionY();

            if (steps == -1) {

                if (rules.isMovePossible(course, activePlayer, steps)) {
                    switch (activePlayer.getViewDirection()) {
                        case 'N':
                            activePlayer.setPositionY(position_y + 1);
                            break;
                        case 'S':
                            activePlayer.setPositionY(position_y - 1);
                            break;
                        case 'W':
                            activePlayer.setPositionX(position_x + 1);
                            break;
                        case 'E':
                            activePlayer.setPositionX(position_x - 1);
                            break;
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                boolean steps_test = false;
                while (steps > 0) {
                    position_x = activePlayer.getPositionX();
                    position_y = activePlayer.getPositionY();
                    if (rules.isMovePossible(course, activePlayer, 1)) { // @Todo steps--, siehe 567 hat gefehlt
                        switch (activePlayer.getViewDirection()) {
                            case 'N':
                                activePlayer.setPositionY(position_y - 1);
                                steps--;
                                break;
                            case 'S':
                                activePlayer.setPositionY(position_y + 1);
                                steps--;
                                break;
                            case 'W':
                                activePlayer.setPositionX(position_x - 1);
                                steps--;
                                break;
                            case 'E':
                                activePlayer.setPositionX(position_x + 1);
                                steps--;
                                break;
                            default: // @Todo
                                System.err.println("Move Zeile 555");
                                steps--;
                        }

                    } else {
                        steps--;
                    }
                    if (steps == 0) {
                        steps_test = true;
                    }

                }
                if (steps_test == true) {
                    System.out.println("move done true"); // @Todo

                    return true;
                } else {
                    System.out.println("move done false"); // @Todo

                    return false;
                }
            }
        } else {
            System.out.println("move done false"); // @Todo

            return false;
        }
    }

    /**
     * Soll angrenzenden Roboter pushen.
     *
     * @param player
     * @return boolean
     */
    public boolean push(Robot player) {
        int position_x = player.getPositionX();
        int position_y = player.getPositionY();
        char viewDirection = player.getViewDirection();

        for (int i = 0; i < RoundBuffer.getCapacity(); i++) {
            Robot tmp = RoundBuffer.peek();
            switch (viewDirection) {
                case 'N':
                    if (position_y - 1 == tmp.getPositionY() || position_x == tmp.getPositionX()) {
                        if (rules.isPushPossible(course, tmp, viewDirection)) {
                            tmp.setPositionY(position_y - 1);
                            move(1);
                        } else {
                            return false;
                        }
                    }
                    break;
                case 'S':
                    if (position_y + 1 == tmp.getPositionY() || position_x == tmp.getPositionX()) {
                        if (rules.isPushPossible(course, tmp, viewDirection)) {
                            tmp.setPositionY(position_y + 1);
                            move(1);
                        } else {
                            return false;
                        }
                    }
                    break;
                case 'W':
                    if (position_y == tmp.getPositionY() || position_x - 1 == tmp.getPositionX()) {
                        if (rules.isPushPossible(course, tmp, viewDirection)) {
                            tmp.setPositionX(position_x - 1);
                            move(1);
                        } else {
                            return false;
                        }
                    }
                    break;
                case 'E':
                    if (position_y == tmp.getPositionY() || position_x + 1 == tmp.getPositionX()) {
                        if (rules.isPushPossible(course, tmp, viewDirection)) {
                            tmp.setPositionX(position_x + 1);
                            move(1);
                        } else {
                            return false;
                        }
                    }
                    break;
            }
        }
        return true;
    }

    /**
     * Fügt einen Player in den Buffer ein und weist ihm eine Farbe zu.
     *
     * @param name
     * @return boolean
     */
    public boolean addPlayer(String name, boolean ki) {

        if (Playerbuffer.getCapacity() != 0) {

            playerCount++;
            Robot neu;
            if (ki == true) {
                neu = new RobotKI(name);
            } else {
                neu = new Robot(name);
            }
            // System.out.println("karamba");
            switch (playerCount) {
                case 1:
                    neu.setColor("Pink");
                    break;
                case 2:
                    neu.setColor("Blue");
                    break;
                case 3:
                    neu.setColor("Green");
                    break;
                case 4:
                    neu.setColor("Orange");
                    break;
                case 5:
                    neu.setColor("White");
                    break;
                case 6:
                    neu.setColor("Cyan");
                    break;
                case 7:
                    neu.setColor("Yellow");
                    break;
                case 8:
                    neu.setColor("Red");
                    break;
            }

            Playerbuffer.push(neu);
            return true;

        } else {
            return false;

        }
    }

    /**
     * fügt Roboter Objekt hinzu und erhöht den Playercounter.
     */

    public boolean addPlayer(Robot robot) {
        if (Playerbuffer.getCapacity() != 0) {

            playerCount++;
            Playerbuffer.push(robot);
            return true;

        } else {
            return false;

        }
    }

    /**
     * Bereitet RoundBuffer vor und prueft, ob genuegend Spieler angemeldet sind.
     * Ruft die Methode placeRobots(), deck() & resetActivePlayer() &
     * dealCardsNewRound() auf.
     *
     * @return String
     */
    public String startGame() {

        if (rules.isStartPossible(Playerbuffer)) {
            RoundBuffer = new Ringbuffer(8 - Playerbuffer.getFreeSpace());

            Robot setFlags;
            while ((setFlags = Playerbuffer.peek()) != null) {
                setFlags.setRobolife(rules.getStartLife());
                setFlags.setFlagNumber(rules.getFlagNumber());
                RoundBuffer.push(setFlags);
            }
            placeRobots();
            deck();
            RoundBuffer.resetRead();
            dealCardsNewRound();
            resetActivePlayer();
            return "";
        }
        return "Too few players";
    }

    /**
     * Iteriert durch das Registry des Spielers. Sofern noch kein Wert hinterlegt
     * ist, wird Priority-Number im Register[] abgelegt. Methode kann dann erneut
     * aufgerufen werden, bis 4 mal geschehen. Achtung! IndexOutOfBoundException!
     * Aufruf mehr als 4 mal muss register wieder komplett auf {0,0,0,0} setzen!
     *
     * @param index = Indexstelle, welche Karte ausgewaehlt wurde.
     * @return boolean
     */
    public boolean chooseCard(int index) {

        if (activePlayer instanceof Robot) {
            activePlayer.chooseCard(index); //@ToDo Fehler beim Spielablauf: "ArrayIndexOutOfBoundsException: 8"

        } else if (activePlayer instanceof RobotKI) {
            ((RobotKI) activePlayer).chooseCardKI();
        }
        int[] registerFill = activePlayer.getRegistry();

        for (int i = 0; i < activePlayer.getRegistry().length; i++) {
            if (activePlayer.getRegistry()[i] == 0) {
                registerFill[i] = activePlayer.getHand()[index];
                activePlayer.setRegistry(registerFill);
                return true;
            }

        }

        return false;
    }

    /**
     * Zielfeld der KI wird identifiziert und im Zusammenhang mit dem aktuellen
     * Leben gesetzt.
     *
     * @return int[] = { yCoord , xCoord }
     */

    /* public int[] setGoalKI() { int flagXCoord = Integer.MAX_VALUE; int flagYCoord
     * = Integer.MAX_VALUE; int repairXCoord = Integer.MAX_VALUE; int repairYCoord =
     * Integer.MAX_VALUE; ArrayList<int[]> repairList = new ArrayList<int[]>();
     *
     * for (int yAchse = 0; yAchse < course.getField().length; yAchse++) { for (int
     * xAchse = 0; xAchse < course.getField()[yAchse].length; xAchse++) {
     *
     * Field fieldFlag = course.getSingleField(yAchse, xAchse); if
     * (activePlayer.getFlagNumber()[0] == 0) { if (fieldFlag.getFullEffects() ==
     * fullEffects.flag1) { flagXCoord = xAchse; flagYCoord = yAchse; } } else if
     * (activePlayer.getFlagNumber()[1] == 0) { if (fieldFlag.getFullEffects() ==
     * fullEffects.flag2) { flagXCoord = xAchse; flagYCoord = yAchse; } } else if
     * (activePlayer.getFlagNumber()[2] == 0) { if (fieldFlag.getFullEffects() ==
     * fullEffects.flag3) { flagXCoord = xAchse; flagYCoord = yAchse; } } else if
     * (activePlayer.getFlagNumber()[3] == 0) { if (fieldFlag.getFullEffects() ==
     * fullEffects.flag4) { flagXCoord = xAchse; flagYCoord = yAchse; } }
     *
     * if (fieldFlag.getFullEffects() == fullEffects.repair) { repairXCoord =
     * xAchse; repairYCoord = yAchse;
     *
     * int[] repair = {repairXCoord, repairYCoord}; repairList.add(repair); }
     *
     * } }
     *
     * int actuellXCoord = activePlayer.getPositionX(); int actuellYCoord =
     * activePlayer.getPositionY();
     *
     * int distanceToFlagX = actuellXCoord - flagXCoord; int distanceToFlagY =
     * actuellYCoord - flagYCoord;
     *
     * int distanceToRepairX = Integer.MAX_VALUE; int distanceToRepairY =
     * Integer.MAX_VALUE;
     *
     * int identifiedSmallX; int identifiedSmallY;
     *
     * for (int[] repair : repairList) {
     *
     * int tempDistanceToRepairX = Integer.MAX_VALUE; int tempDistanceToRepairY =
     * Integer.MAX_VALUE;
     *
     * repairXCoord = repair[0]; repairYCoord = repair[1];
     *
     * tempDistanceToRepairX = actuellXCoord - repairXCoord; tempDistanceToRepairY =
     * actuellYCoord - repairYCoord;
     *
     * if (tempDistanceToRepairX < distanceToRepairX && tempDistanceToRepairY <
     * distanceToRepairY) { identifiedSmallX = repairXCoord; identifiedSmallY =
     * repairYCoord;
     *
     * distanceToRepairX = tempDistanceToRepairX; distanceToRepairY =
     * tempDistanceToRepairY; } } int[] goalCoords = new int[2];
     *
     * if (activePlayer.getRobolife() < 6) { goalCoords[0] = repairYCoord;
     * goalCoords[1] = repairXCoord; } else { goalCoords[0] = flagYCoord;
     * goalCoords[1] = flagXCoord; } return goalCoords; }
     */

    /*** Pr�ft von der position aus die vor der KI liegenden 3 Felder und gibt
     * zur�ck, ob ein Move3 m�glich ist.
     *
     * @return String = "Move3", String = "rotate"
     */
    /*
     * public String check3Fields(char viewDir, int kiYCoord, int kiXCoord) { switch
     * (viewDir) { case 'N': if (course.getSingleField(kiYCoord,
     * kiXCoord).getSideEffects()[0] != sideEffects.wall &&
     * course.getSingleField(kiYCoord - 1, kiXCoord).getSideEffects()[2] !=
     * sideEffects.wall && course.getSingleField(kiYCoord - 1,
     * kiXCoord).getSideEffects()[0] != sideEffects.wall &&
     * course.getSingleField(kiYCoord - 1, kiXCoord).getFullEffects() !=
     * fullEffects.pit && course.getSingleField(kiYCoord - 2,
     * kiXCoord).getSideEffects()[2] != sideEffects.wall &&
     * course.getSingleField(kiYCoord - 2, kiXCoord).getSideEffects()[0] !=
     * sideEffects.wall && course.getSingleField(kiYCoord - 2,
     * kiXCoord).getFullEffects() != fullEffects.pit &&
     * course.getSingleField(kiYCoord - 3, kiXCoord).getSideEffects()[2] !=
     * sideEffects.wall && course.getSingleField(kiYCoord - 3,
     * kiXCoord).getFullEffects() != fullEffects.pit) { return "Move3"; } return
     * "rotate";
     *
     * case 'E': if (course.getSingleField(kiYCoord, kiXCoord).getSideEffects()[1]
     * != sideEffects.wall && course.getSingleField(kiYCoord, kiXCoord +
     * 1).getSideEffects()[3] != sideEffects.wall && course.getSingleField(kiYCoord,
     * kiXCoord + 1).getSideEffects()[1] != sideEffects.wall &&
     * course.getSingleField(kiYCoord, kiXCoord + 1).getFullEffects() !=
     * fullEffects.pit && course.getSingleField(kiYCoord, kiXCoord +
     * 2).getSideEffects()[3] != sideEffects.wall && course.getSingleField(kiYCoord,
     * kiXCoord + 2).getSideEffects()[1] != sideEffects.wall &&
     * course.getSingleField(kiYCoord, kiXCoord + 2).getFullEffects() !=
     * fullEffects.pit && course.getSingleField(kiYCoord, kiXCoord +
     * 3).getSideEffects()[3] != sideEffects.wall && course.getSingleField(kiYCoord,
     * kiXCoord + 3).getFullEffects() != fullEffects.pit) { return "Move3"; } return
     * "rotate"; case 'S': if (course.getSingleField(kiYCoord,
     * kiXCoord).getSideEffects()[2] != sideEffects.wall &&
     * course.getSingleField(kiYCoord + 1, kiXCoord).getSideEffects()[0] !=
     * sideEffects.wall && course.getSingleField(kiYCoord + 1,
     * kiXCoord).getSideEffects()[2] != sideEffects.wall &&
     * course.getSingleField(kiYCoord + 1, kiXCoord).getFullEffects() !=
     * fullEffects.pit && course.getSingleField(kiYCoord + 2,
     * kiXCoord).getSideEffects()[0] != sideEffects.wall &&
     * course.getSingleField(kiYCoord + 2, kiXCoord).getSideEffects()[2] !=
     * sideEffects.wall && course.getSingleField(kiYCoord + 2,
     * kiXCoord).getFullEffects() != fullEffects.pit &&
     * course.getSingleField(kiYCoord + 3, kiXCoord).getSideEffects()[0] !=
     * sideEffects.wall && course.getSingleField(kiYCoord + 3,
     * kiXCoord).getFullEffects() != fullEffects.pit) { return "Move3"; } return
     * "rotate"; case 'W': if (course.getSingleField(kiYCoord,
     * kiXCoord).getSideEffects()[3] != sideEffects.wall &&
     * course.getSingleField(kiYCoord, kiXCoord - 1).getSideEffects()[1] !=
     * sideEffects.wall && course.getSingleField(kiYCoord, kiXCoord -
     * 1).getSideEffects()[3] != sideEffects.wall && course.getSingleField(kiYCoord,
     * kiXCoord - 1).getFullEffects() != fullEffects.pit &&
     * course.getSingleField(kiYCoord, kiXCoord - 2).getSideEffects()[1] !=
     * sideEffects.wall && course.getSingleField(kiYCoord, kiXCoord -
     * 2).getSideEffects()[3] != sideEffects.wall && course.getSingleField(kiYCoord,
     * kiXCoord - 2).getFullEffects() != fullEffects.pit &&
     * course.getSingleField(kiYCoord, kiXCoord - 3).getSideEffects()[1] !=
     * sideEffects.wall && course.getSingleField(kiYCoord, kiXCoord -
     * 3).getFullEffects() != fullEffects.pit) { return "Move3"; } return "rotate";
     *
     * } return null; }
     *
     * /** Identifiziert die bestm�gliche Handkarte und ruft f�r die KI die
     * chooseCard(int index) auf.
     */
    /*
     * @Override public void chooseCardKI() { int[] goalCoords = setGoalKI(); int
     * goalXCoord = goalCoords[0]; int goalYCoord = goalCoords[1];
     *
     * int kiXCoord = activePlayer.getPositionX(); int kiYCoord =
     * activePlayer.getPositionY(); char kiView = activePlayer.getViewDirection();
     * System.out.println("Before resultMove"); String resultMove =
     * check3Fields(kiView, kiYCoord, kiXCoord);
     * System.out.println("After resultMove"); for (int i = 0; i <
     * activePlayer.getRegistry().length; i++) { if (i <= 3) { if
     * (resultMove.equals("Move3")) { System.out.println("Before contains Move3");
     * int cardTmp = containsFromHighest(new int[]{860, 430});
     * System.out.println(cardTmp + " return of containsFromHighest");
     *
     * chooseCard(cardTmp);
     *
     *
     * System.out.println("After contains Move3"); } else if
     * (resultMove.equals("rotate")) { System.out.println("Before contains rotate");
     * chooseCard(contains(new int[]{70, 420}));
     * System.out.println("After contains rotate");
     *
     * System.out.println("Before containsFromHighest Move");
     * chooseCard(containsFromHighest(new int[]{860, 430}));
     * System.out.println("After containsFromHighest Move"); i++; } } }
     * setRegistryReady(); }
     *
     * /** �berpr�ft ob mindestens 1 Karte in dem gesuchten Zahlenbereich in der
     * Hand der KI ist Falls ja, setze hand an der Stelle 0 und return den index an
     * der die Zahl steht.
     *
     * @param priorities
     *
     * @return
     */
    /*
     * public int contains(int[] priorities) { int[] hand = activePlayer.getHand();
     *
     * for (int i = 0; i < hand.length; i++) { for (int j = priorities[0]; j <=
     * priorities[1]; j++) {
     *
     * if (hand[i] == j) { hand[i] = 0; return i; } j += 9; } } return 0;
     *
     * } /* public int containsFromHighest(int[] priorities) { int[] hand =
     * activePlayer.getHand(); int tmp = -1; for (int i = 0; i < hand.length; i++) {
     * for (int j = priorities[0]; j >= priorities[1]; j--) {
     *
     * if (hand[i] == j) {
     *
     * tmp = i; //return; } j -= 9; } } if (tmp != 0) { return tmp; } else { return
     * tmp; }
     *
     *
     * } /* /** überprüft, ob das Register gefüllt ist.
     *
     * @return boolean
     */
    public boolean registryFilled() {
        boolean filled = false;
        for (int i = 0; i <= activePlayer.getRegistry().length - 1; i++) {
            if (activePlayer.getRegistry()[i] != 0) {
                filled = true;
            } else {
                filled = false;
            }
        }
        return filled;
    }

    /**
     * Zeigt die Map als zweidimensionales Array an und vergibt Zeichen für jedes
     * Feld.
     *
     * @return String [][], der die Map zweidimensional ausgibt.
     */
    public String[][] showMap() {
        RoundBuffer.resetRead();

        String[][] mapAsString = new String[course.getField().length][course.getField()[0].length];
        for (int yAchse = 0; yAchse < mapAsString.length; yAchse++) {
            for (int xAchse = 0; xAchse < mapAsString[yAchse].length; xAchse++) {
                for (int d = 0; d < RoundBuffer.getCapacity(); d++) {
                    Robot player = RoundBuffer.peek();
                    int x = player.getPositionX();
                    int y = player.getPositionY();
                    if (yAchse == y && xAchse == x) {
                        mapAsString[yAchse][xAchse] = " " + player.getColor().charAt(0) + " ";
                        continue;
                    } else if (course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.stand) {
                        if (mapAsString[yAchse][xAchse] == null)
                            mapAsString[yAchse][xAchse] = " # ";
                    } else if (course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.pit) {
                        if (mapAsString[yAchse][xAchse] == null)
                            mapAsString[yAchse][xAchse] = " x ";
                    } else if (course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.belt_north) {
                        if (mapAsString[yAchse][xAchse] == null)
                            mapAsString[yAchse][xAchse] = " ^ ";
                    } else if (course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.belt_south) {
                        if (mapAsString[yAchse][xAchse] == null)
                            mapAsString[yAchse][xAchse] = " v ";
                    } else if (course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.belt_east) {
                        if (mapAsString[yAchse][xAchse] == null)
                            mapAsString[yAchse][xAchse] = " > ";
                    } else if (course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.belt_west) {
                        if (mapAsString[yAchse][xAchse] == null)
                            mapAsString[yAchse][xAchse] = " < ";
                    } else if (course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.dock1
                            || course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.dock2
                            || course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.dock3
                            || course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.dock4
                            || course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.dock5
                            || course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.dock6
                            || course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.dock7
                            || course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.dock8) {
                        if (mapAsString[yAchse][xAchse] == null)
                            mapAsString[yAchse][xAchse] = " ~ ";
                    } else if (course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.flag1) {
                        if (mapAsString[yAchse][xAchse] == null)
                            mapAsString[yAchse][xAchse] = " 1 ";
                    } else if (course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.flag2) {
                        if (mapAsString[yAchse][xAchse] == null)
                            mapAsString[yAchse][xAchse] = " 2 ";
                    } else if (course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.flag3) {
                        if (mapAsString[yAchse][xAchse] == null)
                            mapAsString[yAchse][xAchse] = " 3 ";
                    } else if (course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.flag4) {
                        if (mapAsString[yAchse][xAchse] == null)
                            mapAsString[yAchse][xAchse] = " 4 ";
                    } else if (course.getField()[yAchse][xAchse].getFullEffects() == fullEffects.repair) {
                        if (mapAsString[yAchse][xAchse] == null)
                            mapAsString[yAchse][xAchse] = " @ ";
                    }
                }
            }

        }

        String[] test = mapAsString[14];

        for (int i = 0; i < test.length; i++) {
            if (test[i] == null) {
                test[i] = new String(" o ");
            }
        }

        for (String[] arr : mapAsString) {
            for (String s : arr) {
                s += " \n ";
            }
        }

        return mapAsString;
    }

    /**
     * int Priority-Number, sowie String Bezeichnung
     *
     * @return String [] an iController
     */
    public String[] showCards() {
        String[] cards = new String[activePlayer.getHand().length];
        int[] handInInt = activePlayer.getHand();
        for (int i = 0; i < handInInt.length; i++) {
            switch (handInInt[i]) {
                case (10):// Fallthrough UTurn
                case (20):
                case (30):
                case (40):
                case (50):
                case (60):

                    cards[i] = handInInt[i] + " UTurn";
                    break;
                case (70):// Fallthrough RotateLeft
                case (90):
                case (110):
                case (130):
                case (150):
                case (170):
                case (190):
                case (210):
                case (230):
                case (250):
                case (270):
                case (290):
                case (310):
                case (330):
                case (350):
                case (370):
                case (390):
                case (410):
                    cards[i] = handInInt[i] + " RotateLeft";
                    break;
                case (80):// Fallthrough RotateRight
                case (100):
                case (120):
                case (140):
                case (160):
                case (180):
                case (200):
                case (220):
                case (240):
                case (260):
                case (280):
                case (300):
                case (320):
                case (340):
                case (360):
                case (380):
                case (400):
                case (420):
                    cards[i] = handInInt[i] + " RotateRight";
                    break;
                case (430):// Fallthrough backup
                case (440):
                case (450):
                case (460):
                case (470):
                case (480):
                    cards[i] = handInInt[i] + " Backup";
                    break;
                case (490): // Fallthrough move1
                case (500):
                case (510):
                case (520):
                case (530):
                case (540):
                case (550):
                case (560):
                case (570):
                case (580):
                case (590):
                case (600):
                case (610):
                case (620):
                case (630):
                case (640):
                case (650):
                case (660):
                    cards[i] = handInInt[i] + " move1";
                    break;
                case (670):// Fallthrough move2
                case (680):
                case (690):
                case (700):
                case (710):
                case (720):
                case (730):
                case (740):
                case (750):
                case (760):
                case (770):
                case (780):
                    cards[i] = handInInt[i] + " move2";
                    break;
                case (790):// Fallthrough move3
                case (800):
                case (810):
                case (820):
                case (830):
                case (840):
                    cards[i] = handInInt[i] + " move3";
                    break;
            }
        }
        return cards;
    }

    /**
     * Roboter möchte eine Runde aussetzten, kannn aber noch Schaden erhalten.
     */
    public boolean powerDown() {
        if (rules.isPowerDownPossible()) {
            activePlayer.setPower(false);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return Name & Farbe des aktuellen Spielers an iController
     */
    public String[] getPlayernameAndColor() {
        return new String[]{activePlayer.getName(), activePlayer.getColor()};
    }

    /**
     * Soll das Leben des Spielers anzeigen. Bekommt einen String name, sucht im
     * RoundBuffer nach diesem und gibt einen Int wert mit dem Leben zurück
     *
     * @return int
     */
    public int showPlayerlife() {
        return activePlayer.getPlayerlife();
    }

    /**
     * Soll das Leben des Roboters anzeigen. Bekommt einen String farbe, sucht im
     * RoundBuffer nach diesem und gibt einen Int wert mit dem Leben zurück
     *
     * @return int
     */
    public int showRobolife() {
        return activePlayer.getRobolife();
    }

    /**
     * nimmt die Differenz vom Roboterleben und dem Schaden
     *
     * @param player
     * @param damage
     */
    public void dealDamageRoboLife(Robot player, int damage) {
        player.setRobolife(player.getRobolife() - damage);
        System.out.println(player.getRobolife() + " " + player.getColor());
    }

    /**
     * nimmt die Differenz vom Playerleben und dem Schaden.
     *
     * @param player
     */
    public void dealDamagePlayerLife(Robot player) {
        player.setPlayerlife(player.getRobolife() - 1);
    }

    /**
     * Prüft nach jedem Zug, ob das Spiel beendet ist. (wenn alle Flaggen
     * eingesammelt)
     *
     * @return boolean
     */
    public String[] isFinished() {
        int[] flags = activePlayer.getFlagNumber();

        if (flags[flags.length - 1] != 0) { // Gamemaster
            return this.getPlayernameAndColor();
        }
        return null;
    }

    /**
     * Setzt activen Spieler auf den ersten Robot in dem Roundbuffer. Neue Runde!
     */
    public void resetActivePlayer() {
        RoundBuffer.resetRead();
        activePlayer = RoundBuffer.peek();
    }

    /**
     * Setzt die Laenge der Hand in relation zum Leben und sperrt die Registry &
     * Hand
     *
     * @param player
     */
    public void lockRobolifeHandRegistry(Robot player) {

        int actualLife = activePlayer.getRobolife();

        activePlayer.setHandLength(actualLife - 1);

        switch (activePlayer.getRobolife()) {
            case 10:
                activePlayer.setHand(new int[9]);
                break;
            case 9:
                activePlayer.setHand(new int[8]);
                break;
            case 8:
                activePlayer.setHand(new int[7]);
                break;
            case 7:
                activePlayer.setHand(new int[6]);
                break;
            case 6:
                activePlayer.setHand(new int[5]);
                break;
            case 5:
                activePlayer.setHand(new int[4]);
                activePlayer.setRegistry(new int[4]);
                break;
            case 4:
                activePlayer.setHand(new int[3]);
                activePlayer.setRegistry(new int[3]);
                break;
            case 3:
                activePlayer.setHand(new int[2]);
                activePlayer.setRegistry(new int[2]);
                break;
            case 2:
                activePlayer.setHand(new int[1]);
                activePlayer.setRegistry(new int[1]);
                break;
            case 1:
                activePlayer.setRegistry(new int[0]);
                break;
        }
    }

    /**
     *
     */
    public int getRegistryLength() {
        return activePlayer.getRegistry().length;
    }

    public void exitGame() {
        // @TODO
    }

    /**
     * Iteriert durch alle Spieler, ob die richtige Flagge eingesammelt werden kann.
     *
     * @param flagPlace
     * @param player
     * @return boolean
     */
    public boolean isRightFlag(Field flagPlace, Robot player) {
        int[] flag = player.getFlagNumber();
        switch (flagPlace.getFullEffects()) {
            case flag1:
                if (flag[0] == 0) {
                    flag[0] = 1;
                    player.setFlagNumber(flag);
                    return true;
                }
                break;
            case flag2:
                if (flag[0] == 1 || flag[1] == 0) {
                    flag[1] = 2;
                    player.setFlagNumber(flag);
                    return true;
                }
                break;
            case flag3:
                if (flag[0] == 1 || flag[1] == 2 || flag[2] == 0) {
                    flag[2] = 3;
                    player.setFlagNumber(flag);
                    return true;
                }
                break;
            case flag4:
                if (flag[0] == 1 || flag[1] == 2 || flag[2] == 3 || flag[3] == 0) {
                    flag[3] = 4;
                    player.setFlagNumber(flag);
                    return true;
                }
                break;
        }
        return false;
    }

    /**
     * Pr�ft und f�hrt aus: Move durch Belt, Flag, Pit Repair, Laserdamage
     */
    public void fieldAction() {
        Robot player;
        for (int i = 0; i < RoundBuffer.getCapacity(); i++) {
            player = RoundBuffer.peek();
            Field field = course.getSingleField(player.getPositionY(), player.getPositionX());
            switch (field.getFullEffects()) {
                // für die belts fehlt exception bzw. nachricht bei else-Fall
                case belt_north:
                    player.setViewDirection('N');
                    /*
                     * if (rules.isMovePossible(course, player, 1) == true) { move(1);
                     * System.out.println("BeltNorth move 1"); }
                     */
                    player.setPositionY(player.getPositionY() - 1);
                    break;
                case belt_south:
                    player.setViewDirection('S');
                    /*
                     * if (rules.isMovePossible(course, player, 1) == true) { move(1); }
                     */
                    player.setPositionY(player.getPositionY() + 1);
                    break;
                case belt_west:
                    player.setViewDirection('W');
                    /*
                     * if (rules.isMovePossible(course, player, 1) == true) {
                     * System.out.println("BeltWest move 1"); move(1); }
                     */
                    player.setPositionX(player.getPositionX() - 1);
                    break;
                case belt_east:
                    player.setViewDirection('E');
                    /*
                     * if (rules.isMovePossible(course, player, 1) == true) { move(1); }
                     */
                    player.setPositionX(player.getPositionX() + 1);
                    break;
                case flag1: // fallthrough
                case flag2:
                case flag3:
                case flag4:
                    isRightFlag(field, player);
                    break;
                case pit:
                    player.setRobolife(0); // Mit String rückgabe im Gamemaster parsen

                    break;
                case repair:
                    player.setRobolife(player.getRobolife() + 1);
                    break; // Mit String rückgabe im Gamemaster parsen
                case laserdamage:
                    player.setRobolife(player.getRobolife() - 1);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * �?berprüft mit der isRobotLaserPossible(), ob der Roboter einem anderen
     * Schaden zufügen kann. Wenn, return ungleich null dealDamageRoboLife() auf
     * dem von isRobotLaserPossible () übergebenen Roboter mit einem Damage. Wenn
     * Rückgabe null return false
     *
     * @return
     */
    // @Todo später in playRound einfügen. Jeweils für Laser und Roboter
    public boolean damageLineofSightRobot() {

        Robot tmp = rules.isRobotLaserPossible(course, RoundBuffer, activePlayer);

        if (tmp != null) {
            dealDamageRoboLife(tmp, 1);
            return true;
        } else {
            return false;
        }
    }

    /**
     * �?berprüft mit der isLaserPossible(), ob der Roboter einem anderen Schaden
     * zufügen kann. Wenn, return ungleich null dealDamageRoboLife() auf dem von
     * isLaserPossible () übergebenen Roboter mit einem Damage. Wenn Rückgabe null
     * return false
     *
     * @param position_x
     * @param position_y
     * @param direction
     * @return
     */
    // @Todo später in playRound einfügen. Jeweils für Laser und Roboter
    public boolean damageLineofSightLaser(int position_y, int position_x, int direction) {

        Robot tmp = rules.isLaserPossible(course, RoundBuffer, position_y, position_x, direction);

        if (tmp != null) {
            dealDamageRoboLife(tmp, 1);
            return true;
        } else {
            return false;
        }
    }

    public Robot getActiveRobot() {
        return activePlayer;
    }

    public Ringbuffer getRoundBuffer() {
        return RoundBuffer;
    }

    @Override
    public void setRegistryReady() {
        if (registryFilled()) {
            activePlayer.setRegistryReady(true);
            activePlayer = RoundBuffer.peek();
        } else {
            System.out.println("not ready");
        }
    }

    @Override
    public boolean getRegistryReady() {
        return activePlayer.getRegistryReady();
    }

    @Override
    public void setRules(Object rulebook) {
        rules = (defaultRulebook) rulebook;
    }

    public Course getCourse() {
        return course;
    }
}