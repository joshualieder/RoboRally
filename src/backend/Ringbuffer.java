package backend;

/**
 * Ringbuffer speichert alle Spieler
 */
public class Ringbuffer {
    public Robot[] elements = null;

    private int capacity = 0;
    private int writePointer = 0;
    private int freeSpace = 0;
    private int readPointer = 0;

    /**
     * Ringbuffer Konstruktor wirft eine Exception wenn die Größe falsch ist.
     *
     * @Throws IllegalArgumentException wird geworfen wenn die Größe kleiner gleich Null ist
     */
    public Ringbuffer(int capacity) {
        if (capacity <= 0) {
            throw new IllegalArgumentException("Expected getCapacity > 0!");
        }

        this.capacity = capacity;
        this.elements = new Robot[capacity];
        this.freeSpace = capacity;
    }

    /**
     * Setzt den writePointer und getFreeSpace auf null um von vorne damit Arbeiten zu können
     */
    public void reset() {
        this.writePointer = 0;
        this.freeSpace = 0;
    }

    /**
     * Gibt die aktuelle Größe des Ringbuffer zurück
     *
     * @return aktuelle größe Ringbuffer
     */
    public int getCapacity() {
        return this.capacity;
    }

    /**
     * Gibt die Anzahl der Leeren stellen im Ringbuffer zurück
     *
     * @return leere stellen im Ringbuffer
     */
    public int getFreeSpace() {
        return this.freeSpace;
    }

    /**
     * Hängt ein Spielerobjekt im Ringbuffer an. Wenn dieser voll ist, wird das erste Element überschrieben
     *
     * @param element das zu Schreibende Spielerobjekt
     * @return ob das anhängen des Objektes erfolgreich war
     */
    public boolean push(Robot element) {

        if (freeSpace <= capacity) {
            if (writePointer >= capacity) {
                writePointer = 0;
            }
            elements[writePointer] = element;
            writePointer++;
            freeSpace--;
            return true;
        }

        return false;
    }

    /**
     * Gibt ein Spielerobjekt zurück und entfernt diesen aus dem Ringbuffer
     *
     * @return null wenn der Ringbuffer leer ist; nextObj Spielerobejtk
     */
    public Object pop() {
        if (freeSpace == capacity) {
            return null;
        }
        int nextSlot = writePointer - freeSpace;
        if (nextSlot < 0) {
            nextSlot += capacity;
        }
        while (elements[nextSlot] == null)
            nextSlot++;
        Robot nextObj = elements[nextSlot];
        elements[nextSlot] = null;
        freeSpace++;
        return nextObj;
    }

    /**
     * Gibt ein Spielerobjekt nach Reihenfolge zurück ohne Ihn zu entfernen; Springt nach jedem aufruf zum nächsten
     *
     * @return Temporäre Spielerobjekt
     */
    public Robot peek() {
        if (readPointer >= elements.length) {
            readPointer = 0;
        }
        Robot temp = elements[readPointer];
        readPointer++;
        return temp;
    }
    /**
     * Das gleiche wie peek, nur dass der Readpointer nicht erhöt wird.
     * @return
     */
    public Robot peekNoJump() {
    	if (readPointer >= elements.length) {
            readPointer = 0;
        }
        Robot temp = elements[readPointer];
        return temp;
    }

    /**
     * Zeigt an eine stelle im Ringbuffer zum durchiterieren
     *
     * @return stelle des readPointer im Ringbuffer
     */
    public int getReadPointer() {
        return readPointer;
    }

    /**
     * Setzt die aktuelle Stelle des readPointer im Ringbuffer
     */
    public void setReadPointer(int readpointer) {
        this.readPointer = readpointer;
    }

    /**
     * Setzt den readPointer im Ringbuffer auf den Index 0
     */
    public void resetRead() {
        readPointer = 0;
    }


}