package backend;

public class RuleFactoryRejects extends defaultRulebook{

	/**
	 * sets amoutFlags to the nessasary Array.
	 */
	
	
    private int[] amountFlags = {0, 0};
    private int startLife = 8;
    /**
     * Überschreiben der Methode für die Map von Checkmate.
     *
     * @return boolean
     */
    @Override
    public boolean isStartPossible(Ringbuffer Playerbuffer) {
        if (Playerbuffer.getFreeSpace() >= 0 && Playerbuffer.getFreeSpace() <= 3) {
            return true;
        } else
            return false;
    }
    @Override
    public int[] getFlagNumber() {
    	return amountFlags;
    }
    
    @Override
    public int getStartLife() {
    	return startLife;
    }
    
    @Override
    public boolean isPowerDownPossible() {
    	return false;
    }
}
