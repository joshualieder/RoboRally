package backend;

import Field.Field;
import Field.fullEffects;

public abstract class defaultRulebook {

    /**
     * Gibt an wie viele Flaggen auf der Karte sind
     */
    private int[] amountFlags = {0, 0, 0, 0};
    private int startLife = 10;

    /**
     * PrÃ¼ft, ob das Spiel starten kann.
     *
     * @param Playerbuffer Im Playerbuffer liegen alle Spieler drin
     * @return boolean
     */
    public boolean isStartPossible(Ringbuffer Playerbuffer) {

        return Playerbuffer.getFreeSpace() >= 6;
    }

    /**
     * PrÃ¼ft, ob der Roboter sich bewegen kann, indem er Ã¼berprÃ¼ft, ob sich eine
     * Wand in Blickichtung befindet.
     *
     * @param course        Zugriff auf das Spielfeld um die X und Y Koordinate zu bekommen fÃ¼r die ÃœberprÃ¼fung der Felder
     * @param currentPlayer Der aktuell ausgewÃ¤hlte Spieler
     * @param move          Hier wird unterschieden ob der Spieler vorwÃ¤rts oder rÃ¼ckwÃ¤rts laufen soll
     * @return boolean      Wir greifen auf das aktuelle Feld zu, checken nach gehrichtung "wand == null", checken das nÃ¤chste Feld auf "wand == null" und im letzen nach einem pit
     */
    public boolean isMovePossible(Course course, Robot currentPlayer, int move) {
        int position_x = currentPlayer.getPositionX();
        int position_y = currentPlayer.getPositionY();
        //System.out.println(currentPlayer.getViewDirection());
        switch (move) {
            case -1:
                switch (currentPlayer.getViewDirection()) {
                    case 'N':
                        return (course.getSingleField(position_y, position_x).getSideEffects()[2] == null &&
                                course.getSingleField(position_y + 1, position_x).getSideEffects()[0] == null &&
                                course.getSingleField(position_y + 1, position_x).getFullEffects() != fullEffects.pit);
                    case 'S':
                        return (course.getSingleField(position_y, position_x).getSideEffects()[0] == null &&
                                course.getSingleField(position_y - 1, position_x).getSideEffects()[2] == null &&
                                course.getSingleField(position_y - 1, position_x).getFullEffects() != fullEffects.pit);
                    case 'W':
                        return (course.getSingleField(position_y, position_x).getSideEffects()[1] == null &&
                                course.getSingleField(position_y, position_x + 1).getSideEffects()[3] == null &&
                                course.getSingleField(position_y, position_x + 1).getFullEffects() != fullEffects.pit);
                    case 'E':
                        return (course.getSingleField(position_y, position_x).getSideEffects()[3] == null &&
                                course.getSingleField(position_y, position_x - 1).getSideEffects()[1] == null &&
                                course.getSingleField(position_y, position_x - 1).getFullEffects() != fullEffects.pit);
                }
                break;
            case 1:
                switch (currentPlayer.getViewDirection()) {
                    case 'N':
                        return (course.getSingleField(position_y, position_x).getSideEffects()[0] == null &&
                                course.getSingleField(position_y - 1, position_x).getSideEffects()[2] == null &&
                                course.getSingleField(position_y - 1, position_x).getFullEffects() != fullEffects.pit);
                    case 'S':
                        return (course.getSingleField(position_y, position_x).getSideEffects()[2] == null &&
                                course.getSingleField(position_y + 1, position_x).getSideEffects()[0] == null &&
                                course.getSingleField(position_y + 1, position_x).getFullEffects() != fullEffects.pit);
                    case 'W':
                        return (course.getSingleField(position_y, position_x).getSideEffects()[3] == null &&
                                course.getSingleField(position_y, position_x - 1).getSideEffects()[1] == null &&
                                course.getSingleField(position_y, position_x - 1).getFullEffects() != fullEffects.pit);
                    case 'E':
                        return (course.getSingleField(position_y, position_x).getSideEffects()[1] == null &&
                                course.getSingleField(position_y, position_x + 1).getSideEffects()[3] == null &&
                                course.getSingleField(position_y, position_x + 1).getFullEffects() != fullEffects.pit);
                }
                break;
        }
        return false;
    }

    /**
     * ÃœbrprÃ¼ft, ob der Gegner gepusht werden kann. Zuerst wird die X und Y koodinate des Spielers geholt. Im Switchcase wird Ã¼berprÃ¼ft in welcher Richtung der Roboter schaut und dementsprechend die Aktion ausgefÃ¼hrt wenn in der jeweiligen Richtung kein hinderniss existiert.
     *
     * @param course    Zugriff auf das Spielfeld um die X und Y Koordinate zu bekommen fÃ¼r die ÃœberprÃ¼fung der Felder
     * @param tmp       Ein TemporÃ¤re Spieler
     * @param direction Die Richtung in der ÃœberprÃ¼ft werden soll
     * @return boolean  Wir greifen auf das aktuelle Feld zu, checken nach gehrichtung "wand == null" und checken das nÃ¤chste Feld auf "wand == null"
     */
    boolean isPushPossible(Course course, Robot tmp, char direction) {
        int position_x = tmp.getPositionX();
        int position_y = tmp.getPositionY();
        switch (direction) {
            case 'N':
                return (course.getSingleField(position_y, position_x).getSideEffects()[0] == null &&
                        course.getSingleField(position_y - 1, position_x).getSideEffects()[2] == null);
            case 'S':
                return (course.getSingleField(position_y, position_x).getSideEffects()[2] == null &&
                        course.getSingleField(position_y + 1, position_x).getSideEffects()[0] == null);
            case 'W':
                return (course.getSingleField(position_y, position_x).getSideEffects()[3] == null &&
                        course.getSingleField(position_y, position_x - 1).getSideEffects()[1] == null);
            case 'E':
                return (course.getSingleField(position_y, position_x).getSideEffects()[1] == null &&
                        course.getSingleField(position_y, position_x + 1).getSideEffects()[3] == null);
        }
        return false;
    }

    /**
     * ÃœberprÃ¼ft das aktuelle Feld auf einen Roboter und einer Wand in Feuer Richtung.
     * Das nÃ¤chste Feld wird ebenfalls auf WÃ¤nde Ã¼berprÃ¼ft.
     * wenn keine WÃ¤nde vorhanden sind wird mit der Methode checkNextFieldifRobot das nÃ¤chste feld auf einen Roboter Ã¼berprÃ¼ft.
     * das solnge bis der Laser auf eine Wand, einen Roboter oder das Ende des Spielfeldes trifft.
     *
     * @param course
     * @param roundbuffer
     * @param position_x
     * @param position_y
     * @param direction
     * @return
     * @ ToDo erste If Abfrage fÃ¼r alle auslagern
     */
    public Robot isLaserPossible(Course course, Ringbuffer roundbuffer, int position_y, int position_x, int direction) {

        Field fieldlaser = course.getField()[position_y][position_x];
        Robot tmp;

        switch (direction) {
            case 0: //N
                tmp = checkFieldifRobot(position_y, position_x, roundbuffer, null);
                if (tmp != null) {
                    return tmp;
                }
                for (; position_y >= 0; position_y--) {
                    if (course.getSingleField(position_y, position_x).getSideEffects()[0] == null &&
                            course.getSingleField(position_y - 1, position_x).getSideEffects()[2] == null) {
                        tmp = checkFieldifRobot(position_y - 1, position_x, roundbuffer, null);
                        if (tmp != null) {
                            return tmp;
                        }
                    }
                }
            case 1: //E
                tmp = checkFieldifRobot(position_y, position_x, roundbuffer, null);
                if (tmp != null) {
                    return tmp;
                }
                for (; position_x <= course.getField()[position_x].length; position_x++) {
                    if (course.getSingleField(position_y, position_x).getSideEffects()[1] == null &&
                            course.getSingleField(position_y, position_x + 1).getSideEffects()[3] == null) {
                        tmp = checkFieldifRobot(position_y, position_x + 1, roundbuffer, null);
                        if (tmp != null) {
                            return tmp;
                        }
                    }
                }
            case 2: //S
                tmp = checkFieldifRobot(position_y, position_x, roundbuffer, null);
                if (tmp != null) {
                    return tmp;
                }
                for (; position_y <= course.getField()[position_y].length; position_y++) {
                    if (course.getSingleField(position_y, position_x).getSideEffects()[2] == null &&
                            course.getSingleField(position_y + 1, position_x).getSideEffects()[0] == null) {
                        tmp = checkFieldifRobot(position_y + 1, position_x, roundbuffer, null);
                        if (tmp != null) {
                            return tmp;
                        }
                    }
                }
            case 3: //W
                tmp = checkFieldifRobot(position_y, position_x, roundbuffer, null);
                if (tmp != null) {
                    return tmp;
                }
                for (; position_x >= 0; position_x--) {
                    if (course.getSingleField(position_y, position_x).getSideEffects()[3] == null &&
                            course.getSingleField(position_y, position_x - 1).getSideEffects()[1] == null) {
                        tmp = checkFieldifRobot(position_y, position_x - 1, roundbuffer, null);
                        if (tmp != null) {
                            return tmp;
                        }
                    }

                }
        }
        return null;
    }

    /**
     * ÃœberprÃ¼ft ob und wann ein Laser auf einen Spieler trifft. Wird noch ausgebaut!
     *
     * @param //course        Zugriff auf das Spielfeld um die X und Y Koordinate zu bekommen fÃ¼r die ÃœberprÃ¼fung der Felder
     * @param //currentPlayer Der aktuelle Spieler
     * @param //direction     Die Richtung in der ÃœberprÃ¼ft werden soll
     * @param //roundbuffer   Der Ringpuffer beinhaltet alles Spieler
     * @return boolean
     */
    public Robot isRobotLaserPossible(Course course, Ringbuffer roundbuffer, Robot currentPlayer) {

        int position_x = currentPlayer.getPositionX();
        int position_y = currentPlayer.getPositionY();
        char direction = currentPlayer.getViewDirection();

        switch (direction) {
            case 'N':
                for (; position_y > 0; position_y--) {
                    if (course.getSingleField(position_y, position_x).getSideEffects()[0] == null &&
                            course.getSingleField(position_y - 1, position_x).getSideEffects()[2] == null) {
                        Robot tmp = checkFieldifRobot(position_y - 1, position_x, roundbuffer, currentPlayer);
                        if (tmp != null) {
                            System.out.println("------------>" + currentPlayer.getColor());
                            return tmp;
                        }
                    }
                }

            case 'S':

                for (; position_y <= course.getField()[position_y].length; position_y++) {
                    if (course.getSingleField(position_y, position_x).getSideEffects()[2] == null &&
                            course.getSingleField(position_y + 1, position_x).getSideEffects()[0] == null) {
                        Robot tmp = checkFieldifRobot(position_y + 1, position_x, roundbuffer, currentPlayer);
                        if (tmp != null) {
                            return tmp;
                        }
                    }
                }

            case 'W':
                for (; position_x > 0; position_x--) {

                    if (course.getSingleField(position_y, position_x).getSideEffects()[3] == null &&
                            course.getSingleField(position_y, position_x - 1).getSideEffects()[1] == null) {
                        Robot tmp = checkFieldifRobot(position_y, position_x - 1, roundbuffer, currentPlayer);
                        if (tmp != null) {
                            return tmp;
                        }
                    }

                }

            case 'E':
                for (; position_x >= course.getField()[position_x].length; position_x++) {
                    if (course.getSingleField(position_y, position_x).getSideEffects()[1] == null &&
                            course.getSingleField(position_y, position_x + 1).getSideEffects()[3] == null) {
                        Robot tmp = checkFieldifRobot(position_y, position_x + 1, roundbuffer, currentPlayer);
                        if (tmp != null) {
                            return tmp;
                        }
                    }
                }
        }
        return null;
    }

    int[] getAmountFlags() {
        return amountFlags;
    }

    /**
     * Mittels der Ãœbergabeparameter wird Ã¼berprÃ¼ft, ob auf der Position ein Roboter steht. Null wenn nicht.
     * Robot mit tmp im return, wenn einer gefunden wird
     *
     * @param position_x
     * @param position_y
     * @param Playerbuffer
     * @return
     */
    private Robot checkFieldifRobot(int position_y, int position_x, Ringbuffer Playerbuffer, Robot currentPlayer) {
        for (int i = 0; i < Playerbuffer.getCapacity(); i++) {
            Robot tmp = (Robot) Playerbuffer.peek();
            if (tmp.getPositionX() == position_x && tmp.getPositionY() == position_y && tmp != currentPlayer) {
                return tmp;
            }
        }
        return null;
    }

    public int[] getFlagNumber() {
        return amountFlags;
    }

    public boolean isPowerDownPossible() {
        return false;
    }

    public int getStartLife() {
        return startLife;
    }

}