package backend;

import java.util.ArrayList;

import Field.Field;
import Field.fullEffects;
import Field.sideEffects;

public class RobotKI extends Robot {

	private Course course = new Course();
	int[] goal;
	int[] handCopy;

	public void handCopyFill() {
		this.handCopy = new int[this.getHand().length];
		for (int i = 0; i < getHand().length; i++) {
			this.handCopy[i] = getHand()[i];
			System.out.print(this.handCopy[i] + "  ");
		}
	}

	public RobotKI(String name) {
		super(name);
	}

	public void findGoalKI() {
		int flagXCoord = Integer.MAX_VALUE;
		int flagYCoord = Integer.MAX_VALUE;
		int repairXCoord = Integer.MAX_VALUE;
		int repairYCoord = Integer.MAX_VALUE;
		ArrayList<int[]> repairList = new ArrayList<int[]>();

		for (int yAchse = 0; yAchse < course.getField().length; yAchse++) {
			for (int xAchse = 0; xAchse < course.getField()[yAchse].length; xAchse++) {

				Field fieldFlag = course.getSingleField(yAchse, xAchse);
				if (this.getFlagNumber()[0] == 0) {
					if (fieldFlag.getFullEffects() == fullEffects.flag1) {
						flagXCoord = xAchse;
						flagYCoord = yAchse;
					}
				} else if (this.getFlagNumber()[1] == 0) {
					if (fieldFlag.getFullEffects() == fullEffects.flag2) {
						flagXCoord = xAchse;
						flagYCoord = yAchse;
					}
				} else if (this.getFlagNumber()[2] == 0) {
					if (fieldFlag.getFullEffects() == fullEffects.flag3) {
						flagXCoord = xAchse;
						flagYCoord = yAchse;
					}
				} else if (this.getFlagNumber()[3] == 0) {
					if (fieldFlag.getFullEffects() == fullEffects.flag4) {
						flagXCoord = xAchse;
						flagYCoord = yAchse;
					}
				}

				if (fieldFlag.getFullEffects() == fullEffects.repair) {
					repairXCoord = xAchse;
					repairYCoord = yAchse;

					int[] repair = { repairXCoord, repairYCoord };
					repairList.add(repair);
				}

			}
		}

		int actuellXCoord = this.getPositionX();
		int actuellYCoord = this.getPositionY();

		int distanceToFlagX = actuellXCoord - flagXCoord;
		int distanceToFlagY = actuellYCoord - flagYCoord;

		int distanceToRepairX = Integer.MAX_VALUE;
		int distanceToRepairY = Integer.MAX_VALUE;

		int identifiedSmallX;
		int identifiedSmallY;

		for (int[] repair : repairList) {

			int tempDistanceToRepairX = Integer.MAX_VALUE;
			int tempDistanceToRepairY = Integer.MAX_VALUE;

			repairXCoord = repair[0];
			repairYCoord = repair[1];

			tempDistanceToRepairX = actuellXCoord - repairXCoord;
			tempDistanceToRepairY = actuellYCoord - repairYCoord;

			if (tempDistanceToRepairX < distanceToRepairX && tempDistanceToRepairY < distanceToRepairY) {
				identifiedSmallX = repairXCoord;
				identifiedSmallY = repairYCoord;

				distanceToRepairX = tempDistanceToRepairX;
				distanceToRepairY = tempDistanceToRepairY;
			}
		}
		int[] goalCoords = new int[2];

		if (this.getRobolife() < 6) {
			goalCoords[0] = repairYCoord;
			goalCoords[1] = repairXCoord;
		} else {
			goalCoords[0] = flagYCoord;
			goalCoords[1] = flagXCoord;
		}
		setGoal(goalCoords);
	}

	public void setGoal(int[] goal) {
		this.goal = goal;
	}

	/**
	 * Algorithmus der KI f�r eine intelligente Kartenauswahl. F�r jedes
	 * Register gilt: Position, bzw. theoretische Position; Drei Felder weiter
	 * schauen ob pit oder wall Ansonsten in bereits vorhandenen if Abfrage
	 *
	 * UTurn = 10 - 60, in 10-er Schritten rotateLeft = 70 - 420, in 20-er Schritten
	 * rotateRight = 80 - 410, in 20-er Schritten backUp = 430 - 480, in 10-er
	 * Schritten move1-3 = 490 - 840 in 10-er Schritten
	 */
	public void chooseCardKI() {
		findGoalKI();
		handCopyFill();
		int goalXCoord = goal[0];
		int goalYCoord = goal[1];
		int priority[];
		// check 3 fields
		// register 1: chooseForXCoord
		//

		for (int i = 0; i < this.getRegistry().length; i++) {

			if (i == 0) {
				if (check3Fields(this.getViewDirection(), this.getPositionY(), this.getPositionX()).equals("Move3")) {
					chooseCard(containsMove(new int[] { 840, 10 }));

				} else {
					// move wird aufgerufen, da rotate Richtung egal
					chooseCard(containsRotate10(new int[] { 420, 70 }));

				}
			} else if (i == 1) {
				chooseForYCoord(goalXCoord, goalYCoord);
			} else if (i == 2) {
				chooseForYCoord(goalXCoord, goalYCoord);
			} else if (i == 3) {
				chooseForXCoord(goalXCoord, goalYCoord);
			} else if (i == 4) {
				chooseForXCoord(goalXCoord, goalYCoord);
			}
		}
	}

	public void chooseForXCoord(int goalXCoord, int goalYCoord) {
		int tmp;
		if (this.getPositionX() < goalXCoord) {

			switch (this.getViewDirection()) {
			case 'N':
				chooseCard(containsRotate20(new int[] { 80, 420 }));
				break;
			case 'E':
				tmp = containsMove(new int[] { 840, 490 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 840, 10 });
				}
				chooseCard(tmp);
				break;
			case 'S':
				chooseCard(containsRotate20(new int[] { 70, 420 }));
				break;
			case 'W':
				tmp = containsMove(new int[] { 60, 10 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 480, 430 });
					if (tmp == -1) {
						tmp = containsMove(new int[] { 840, 10 });
					}
				}
				chooseCard(tmp);

				break;
			}
		} else if (this.getPositionX() > goalXCoord) {
			switch (this.getViewDirection()) {
			case 'N':
				chooseCard(containsRotate20(new int[] { 70, 420 }));
				break;
			case 'E':
				tmp = containsMove(new int[] { 60, 10 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 480, 430 });
					if (tmp == -1) {
						tmp = containsMove(new int[] { 840, 10 });
					}
				}
				chooseCard(tmp);

				break;
			case 'S':
				chooseCard(containsRotate20(new int[] { 80, 410 }));
				break;
			case 'W':
				tmp = containsMove(new int[] { 840, 490 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 840, 10 });
				}
				chooseCard(tmp);
				break;

			}
		} else if (this.getPositionX() == goalXCoord && this.getPositionY() < goalYCoord) {
			switch (this.getViewDirection()) {
			case 'N':
				tmp= containsMove(new int[] {840, 490}); 
				if(tmp ==-1) {
					tmp = containsMove(new int[] {840, 10});
				}
				chooseCard(tmp);
				break;
			case 'E':
				chooseCard(containsRotate20(new int[] { 70, 420 }));
				break;
			case 'S':
				tmp = containsMove(new int[] { 60, 10 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 480, 430 });
					if (tmp == -1) {
						tmp = containsMove(new int[] { 840, 10 });
					}
				}
				chooseCard(tmp);

				break;
			case 'W':
				chooseCard(containsRotate20(new int[] { 80, 410 }));
				break;
			}
		} else if (this.getPositionX() == goalXCoord && this.getPositionY() > goalYCoord) {
			switch (this.getViewDirection()) {
			case 'N':

				tmp = containsMove(new int[] { 60, 10 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 480, 430 });
					if (tmp == -1) {
						tmp = containsMove(new int[] { 840, 10 });
					}
				}
				chooseCard(tmp);

				break;
			case 'E':
				chooseCard(containsRotate20(new int[] { 70, 420 }));
				break;
			case 'S':
				tmp= containsMove(new int[] {840, 490}); 
				if(tmp ==-1) {
					tmp = containsMove(new int[] {840, 10});
				}
				chooseCard(tmp);
				break;
			case 'W':
				chooseCard(containsRotate20(new int[] { 80, 410 }));
				break;
			}
		} else {
			chooseCard(containsMove(new int[] { 840, 10 }));
		}
	}

	// Abfrage, auf die selbe Y Coordinate zu kommen wie das Ziel

	public void chooseForYCoord(int goalXCoord, int goalYCoord) {
		int tmp;
		if (this.getPositionY() < goalYCoord) {

			switch (this.getViewDirection()) {
			case 'N':
				chooseCard(containsRotate20(new int[] { 80, 410 }));
				break;
			case 'E':
				tmp = containsMove(new int[] { 840, 490 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 840, 10 });
				}
				chooseCard(tmp);
				break;

			case 'S':
				chooseCard(containsRotate20(new int[] { 70, 420 }));
				break;
			case 'W':
				tmp = containsMove(new int[] { 60, 10 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 480, 430 });
					if (tmp == -1) {
						tmp = containsMove(new int[] { 840, 10 });
					}
				}
				chooseCard(tmp);
				break;

			}
		} else if (this.getPositionY() > goalYCoord) {
			switch (this.getViewDirection()) {
			case 'N':
				chooseCard(containsRotate20(new int[] { 70, 420 }));
				break;
			case 'E':
				tmp = containsMove(new int[] { 60, 10 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 480, 430 });
					if (tmp == -1) {
						tmp = containsMove(new int[] { 840, 10 });
					}
				}
				chooseCard(tmp);
				break;
			case 'S':
				chooseCard(containsRotate20(new int[] { 80, 410 }));
				break;
			case 'W':
				tmp = containsMove(new int[] { 840, 490 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 840, 10 });
				}
				chooseCard(tmp);
				break;

			}
		} else if (this.getPositionY() == goalYCoord && this.getPositionX() < goalXCoord) {
			switch (this.getViewDirection()) {
			case 'N':
				tmp = containsMove(new int[] { 840, 490 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 840, 10 });
				}
				chooseCard(tmp);
				break;
			case 'E':
				chooseCard(containsRotate20(new int[] { 70, 420 }));
				break;
			case 'S':
				tmp = containsMove(new int[] { 60, 10 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 480, 430 });
					if (tmp == -1) {
						tmp = containsMove(new int[] { 840, 10 });
					}
				}
				chooseCard(tmp);
				break;
			case 'W':
				chooseCard(containsRotate20(new int[] { 80, 410 }));
				break;
			}
		} else if (this.getPositionX() == goalXCoord && this.getPositionX() > goalXCoord) {
			switch (this.getViewDirection()) {
			case 'N':
				tmp = containsMove(new int[] { 60, 10 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 480, 430 });
					if (tmp == -1) {
						tmp = containsMove(new int[] { 840, 10 });
					}
				}
				chooseCard(tmp);
				break;
			case 'E':
				chooseCard(containsRotate20(new int[] { 70, 420 }));
				break;
			case 'S':
				tmp = containsMove(new int[] { 840, 490 });
				if (tmp == -1) {
					tmp = containsMove(new int[] { 840, 10 });
				}
				chooseCard(tmp);
			case 'W':
				chooseCard(containsRotate20(new int[] { 80, 410 }));
				break;
			}
		} else {
			chooseCard(containsMove(new int[] { 840, 10 }));
		}
	}

	/**
	 * �berpr�ft ob mindestens 1 Karte in dem gesuchten Zahlenbereich in der
	 * Hand der KI ist Falls ja, setze hand an der Stelle 0 und return den index an
	 * der die Zahl steht.
	 *
	 * @param priorities
	 * @return
	 */

	public int containsRotate20(int[] priorities) {
		int tmp = -1;

		for (int j = priorities[0]; j <= priorities[1]; j++) {
			for (int i = 0; i < handCopy.length; i++) {
				// System.out.println(j + " " + handCopy[i]);
				if (handCopy[i] == j) {
					handCopy[i] = 0;
					tmp = i;
					return i;

				}

			}
			j += 19;
		}

		if (tmp != -1) {
			return tmp;
		} else {
			return containsMove(new int[] { 840, 10 });
		}

	}
	public int containsRotate10(int[] priorities) {
		int tmp = -1;

		for (int j = priorities[0]; j <= priorities[1]; j++) {
			for (int i = 0; i < handCopy.length; i++) {
				// System.out.println(j + " " + handCopy[i]);
				if (handCopy[i] == j) {
					handCopy[i] = 0;
					tmp = i;
					return i;

				}

			}
			j += 9;
		}

		if (tmp != -1) {
			return tmp;
		} else {
			return containsMove(new int[] { 840, 10 });
		}
	}
	public int containsMove(int[] priorities) {
		int tmp = -1;

		for (int j = priorities[0]; j >= priorities[1]; j--) {
			for (int i = 0; i < handCopy.length; i++) {
				// System.out.println(j + " " + handCopy[i]);
				if (handCopy[i] == j) {
					handCopy[i] = 0;
					tmp = i;
					return i;

				}

			}
			j -= 9;
		}

		if (tmp != -1) {
			return tmp;
		} else {
			return -1;
		}

	}

	/**
	 * Pr�ft von der position aus die vor der KI liegenden 3 Felder und gibt
	 * zur�ck, ob ein Move3 m�glich ist.
	 *
	 * @param viewDir
	 * @param kiYCoord
	 * @param kiXCoord
	 * @return String = "Move3", String = "rotate"
	 */
	public String check3Fields(char viewDir, int kiYCoord, int kiXCoord) {
		switch (viewDir) {
		case 'N':
			if (course.getSingleField(kiYCoord, kiXCoord).getSideEffects()[0] != sideEffects.wall
					&& course.getSingleField(kiYCoord - 1, kiXCoord).getSideEffects()[2] != sideEffects.wall
					&& course.getSingleField(kiYCoord - 1, kiXCoord).getSideEffects()[0] != sideEffects.wall
					&& course.getSingleField(kiYCoord - 1, kiXCoord).getFullEffects() != fullEffects.pit
					&& course.getSingleField(kiYCoord - 2, kiXCoord).getSideEffects()[2] != sideEffects.wall
					&& course.getSingleField(kiYCoord - 2, kiXCoord).getSideEffects()[0] != sideEffects.wall
					&& course.getSingleField(kiYCoord - 2, kiXCoord).getFullEffects() != fullEffects.pit
					&& course.getSingleField(kiYCoord - 3, kiXCoord).getSideEffects()[2] != sideEffects.wall
					&& course.getSingleField(kiYCoord - 3, kiXCoord).getFullEffects() != fullEffects.pit) {
				return "Move3";
			}
			return "rotate";

		case 'E':
			if (course.getSingleField(kiYCoord, kiXCoord).getSideEffects()[1] != sideEffects.wall
					&& course.getSingleField(kiYCoord, kiXCoord + 1).getSideEffects()[3] != sideEffects.wall
					&& course.getSingleField(kiYCoord, kiXCoord + 1).getSideEffects()[1] != sideEffects.wall
					&& course.getSingleField(kiYCoord, kiXCoord + 1).getFullEffects() != fullEffects.pit
					&& course.getSingleField(kiYCoord, kiXCoord + 2).getSideEffects()[3] != sideEffects.wall
					&& course.getSingleField(kiYCoord, kiXCoord + 2).getSideEffects()[1] != sideEffects.wall
					&& course.getSingleField(kiYCoord, kiXCoord + 2).getFullEffects() != fullEffects.pit
					&& course.getSingleField(kiYCoord, kiXCoord + 3).getSideEffects()[3] != sideEffects.wall
					&& course.getSingleField(kiYCoord, kiXCoord + 3).getFullEffects() != fullEffects.pit) {
				return "Move3";
			}
			return "rotate";
		case 'S':
			if (course.getSingleField(kiYCoord, kiXCoord).getSideEffects()[2] != sideEffects.wall
					&& course.getSingleField(kiYCoord + 1, kiXCoord).getSideEffects()[0] != sideEffects.wall
					&& course.getSingleField(kiYCoord + 1, kiXCoord).getSideEffects()[2] != sideEffects.wall
					&& course.getSingleField(kiYCoord + 1, kiXCoord).getFullEffects() != fullEffects.pit
					&& course.getSingleField(kiYCoord + 2, kiXCoord).getSideEffects()[0] != sideEffects.wall
					&& course.getSingleField(kiYCoord + 2, kiXCoord).getSideEffects()[2] != sideEffects.wall
					&& course.getSingleField(kiYCoord + 2, kiXCoord).getFullEffects() != fullEffects.pit
					&& course.getSingleField(kiYCoord + 3, kiXCoord).getSideEffects()[0] != sideEffects.wall
					&& course.getSingleField(kiYCoord + 3, kiXCoord).getFullEffects() != fullEffects.pit) {
				return "Move3";
			}
			return "rotate";
		case 'W':
			if (course.getSingleField(kiYCoord, kiXCoord).getSideEffects()[3] != sideEffects.wall
					&& course.getSingleField(kiYCoord, kiXCoord - 1).getSideEffects()[1] != sideEffects.wall
					&& course.getSingleField(kiYCoord, kiXCoord - 1).getSideEffects()[3] != sideEffects.wall
					&& course.getSingleField(kiYCoord, kiXCoord - 1).getFullEffects() != fullEffects.pit
					&& course.getSingleField(kiYCoord, kiXCoord - 2).getSideEffects()[1] != sideEffects.wall
					&& course.getSingleField(kiYCoord, kiXCoord - 2).getSideEffects()[3] != sideEffects.wall
					&& course.getSingleField(kiYCoord, kiXCoord - 2).getFullEffects() != fullEffects.pit
					&& course.getSingleField(kiYCoord, kiXCoord - 3).getSideEffects()[1] != sideEffects.wall
					&& course.getSingleField(kiYCoord, kiXCoord - 3).getFullEffects() != fullEffects.pit) {
				return "Move3";
			}
			return "rotate";

		}
		return null;
	}
}
