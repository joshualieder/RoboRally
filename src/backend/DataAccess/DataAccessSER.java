package backend.DataAccess;

import backend.Ringbuffer;
import backend.Robot;

import java.io.*;
import java.util.HashMap;

public class DataAccessSER implements iDataAccess {

    public void save(Object object, String path) {

        Ringbuffer Playerbuffer = (Ringbuffer) object;
        HashMap<Integer, Robot> saved = new HashMap<Integer, Robot>();

        for (int i = 0; i < Playerbuffer.getCapacity(); i++) {
            Robot robot = (Robot) Playerbuffer.peek();
            saved.put(i, robot);
        }

        try {
            FileOutputStream file_output = new FileOutputStream(path);
            ObjectOutputStream object_output = new ObjectOutputStream(file_output);
            object_output.writeObject(saved);
            object_output.close();
            file_output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Object load(String path) {

        HashMap<Integer, Robot> saved = new HashMap<Integer, Robot>();
        try {
            FileInputStream file_input = new FileInputStream(path);
            ObjectInputStream object_input = new ObjectInputStream(file_input);
            saved = (HashMap<Integer, Robot>) object_input.readObject();
            object_input.close();
            file_input.close();
        } catch (IOException | ClassNotFoundException ioe) {
            ioe.printStackTrace();
        }

        return saved;
    }

}
