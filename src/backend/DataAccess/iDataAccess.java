package backend.DataAccess;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface iDataAccess {
	
	public void save(Object object, String path) throws RuntimeException, FileNotFoundException;
	// kann man hier den Dateizeiger auf Anfang setten? (seek)
	// was genau ist flush?
	// und wo kommt eigentlich der FileInputStream Reader mit FileChooser genau hin?
	
	public Object load(String path) throws FileNotFoundException, IOException;
	// und wo kommt der FileOutputStreamWriter mit FilChooser hin?
}
