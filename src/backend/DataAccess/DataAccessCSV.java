package backend.DataAccess;

import java.io.*;

import backend.Gamemaster;
import backend.Ringbuffer;
import backend.Robot;
import frontend.iController;

public class DataAccessCSV implements iDataAccess {
    //Pfad und Dateiname ber FileChooser
    //Nur so viel speichern, damit das Spielbrett ber JSON ergnzt werden kann, d.h. Roboter-Infos


    @Override
    public void save(Object object, String path) throws FileNotFoundException {
        //List<String> saveData = new ArrayList<String>();
        Ringbuffer Playerbuffer = (Ringbuffer) object;

        PrintWriter pw = new PrintWriter(new File(path));
        StringBuilder saveData = new StringBuilder();

        //Schleife iteriert durch den Playerbuffer, der alle Spieler der akutellen Spiels hlt
        //und speichert die Werte in die saveData-List

        // CSV mit saveData-List erstellen und speichern mit FileChooser
        for (int i = 0; i < Playerbuffer.getCapacity(); i++) {
            Robot tmp = (Robot) Playerbuffer.peek();
            int flag_number = tmp.getFlagNumber().length;

            saveData.append(i + ",");
            saveData.append(tmp.getName() + ",");
            saveData.append(tmp.getColor() + ",");
            saveData.append(tmp.getPlayerlife() + ",");
            saveData.append(tmp.getPositionX() + ",");
            saveData.append(tmp.getPositionY() + ",");
            saveData.append(tmp.getRobolife() + ",");
            saveData.append(tmp.getViewDirection() + ",");
            for (int j = 0; j < tmp.getFlagNumber().length; j++) {
                saveData.append(tmp.getFlagNumber()[j] + ",");
            }
            while (flag_number < 4) {
                saveData.append(0 + ",");
                flag_number++;
            }
            for (int j = 0; j < tmp.getHand().length; j++) {
                saveData.append(tmp.getHand()[j] + ",");
            }
            for (int j = 0; j < tmp.getRegistry().length; j++) {
                saveData.append(tmp.getRegistry()[j] + ",");
            }
            saveData.append(tmp.getSpawnLocation() + ",");
            saveData.append("\n");
            //@Todo welches Spielbrett weil richtig laden und so!11!

        }

        pw.write(saveData.toString());
        pw.close();


    }

    @Override
    public Object load(String path) throws IOException {


        BufferedReader reader = new BufferedReader(new FileReader(path));
        iController Spielleiter = new Gamemaster();
        String[] line = reader.readLine().split(",");
        while (line != null) {
        	System.out.println(line[2]);
            Robot robot = new Robot(line[1]);
            robot.setColor(line[2]);
            robot.setPlayerlife(Integer.valueOf(line[3]));
            robot.setPositionX(Integer.valueOf(line[4]));
            robot.setPositionY(Integer.valueOf(line[5]));
            robot.setRobolife(Integer.valueOf(line[6]));
            robot.setViewDirection(line[7].charAt(0));
            int[] flags = new int[4];
            for (int i = 0; i < 4; i++) {
                flags[i] = Integer.parseInt(line[8 + i]);
            }
            robot.setFlagNumber(flags);

            int[] cards = new int[9];
            for (int i = 0; i < 9; i++) {
                cards[i] = Integer.parseInt(line[12 + i]);
            }
            robot.setRegistry(cards);

            int[] handcards = new int[5];
            for (int i = 0; i < 5; i++) {
                handcards[i] = Integer.parseInt(line[21 + i]);
            }
            robot.setHand(handcards);

            Spielleiter.addPlayer(robot);
            
            String line_next = reader.readLine();
            if (line_next == null) {
                return null;
            } else {
                line = line_next.split(",");
            }
            
        }
        
        return null;
    }
}
