package backend.DataAccess;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import Field.Field;
import Field.fullEffects;
import Field.sideEffects;
import backend.Course;

public class DataAccessJSON implements iDataAccess {

	private String fieldType;
	private int flag;
	private int[] walls = { 0, 0, 0, 0 };
	private int[] lasers = { 0, 0, 0, 0 };
	private int[] rotate = { 0, 0 }; // ToDo
	private int[] shift = { 0, 0, 0, 0 };

	private sideEffects[] sideEffect = new sideEffects[4];
	private fullEffects fullEffect;

	@Override
	public void save(Object object, String path) throws RuntimeException, FileNotFoundException {
		Course course = (Course) object;
		Collection<ArrayList> JsonSaveArrayList = new ArrayList<ArrayList>();
		Field field = new Field(null, null, null, null, null);

		for (int i = 0; i < course.getField().length; i++) {
			ArrayList ArrayZeile = new ArrayList<JsonSaveObject>();
			int walls[] = { 0, 0, 0, 0 };
			int flag = 0;
			for (int j = 0; j < course.getField()[0].length; j++) {
				field = course.getSingleField(j, i);
				JsonSaveObject Object4Array = new JsonSaveObject();

				for (int k = 0; k < 4; k++) {
					if (field.getSideEffects()[k] != null) {
						// Die Abfrage sideEffects.wall scheint nicht zu funktionieren
						if (field.getSideEffects()[k] == sideEffects.wall) {
							System.out.println("Dieses Feld hat eine Wall an Stelle " + k);
							walls[k] = 1;
							Object4Array.setWalls(walls);

						} else if (field.getSideEffects()[k] == sideEffects.laser) {
							lasers[k] = 1;
							Object4Array.setLasers(lasers);
						}
					}
				}
				if (field.getFullEffects() == fullEffects.belt_east || field.getFullEffects() == fullEffects.belt_north
						|| field.getFullEffects() == fullEffects.belt_west
						|| field.getFullEffects() == fullEffects.belt_south) {
					Object4Array.setFieldtype("ConveyorBelt");
					if (field.getFullEffects() == fullEffects.belt_east) {
						Object4Array.setShift(new int[] { 0, 1, 0, 0 });
					} else if (field.getFullEffects() == fullEffects.belt_south) {
						Object4Array.setShift(new int[] { 0, 0, 1, 0 });
					} else if (field.getFullEffects() == fullEffects.belt_west) {
						Object4Array.setShift(new int[] { 0, 0, 0, 1 });
					} else if (field.getFullEffects() == fullEffects.belt_north) {
						Object4Array.setShift(new int[] { 1, 0, 0, 0 });
					}
				} else if (field.getFullEffects() == fullEffects.stand) {
					Object4Array.setFieldtype("Floor");
				} else if (field.getFullEffects() == fullEffects.flag1) {
					flag = 1;
					Object4Array.setFlag(flag);
				} else if (field.getFullEffects() == fullEffects.flag2) {
					flag = 2;
					Object4Array.setFlag(flag);
				} else if (field.getFullEffects() == fullEffects.flag3) {
					flag = 3;
					Object4Array.setFlag(flag);
				} else if (field.getFullEffects() == fullEffects.flag4) {
					flag = 4;
					Object4Array.setFlag(flag);

				} else if (field.getFullEffects() == fullEffects.repair) {
					Object4Array.setFieldtype("Repair");
				} else if (field.getFullEffects() == fullEffects.pit) {
					Object4Array.setFieldtype("Pit");
				} else if (field.getFullEffects() == fullEffects.dock1 || field.getFullEffects() == fullEffects.dock2
						|| field.getFullEffects() == fullEffects.dock3 || field.getFullEffects() == fullEffects.dock4
						|| field.getFullEffects() == fullEffects.dock5 || field.getFullEffects() == fullEffects.dock6
						|| field.getFullEffects() == fullEffects.dock7 || field.getFullEffects() == fullEffects.dock8) {
					if (field.getFullEffects() == fullEffects.dock1)
						Object4Array.setFieldtype("dock1");
					else if (field.getFullEffects() == fullEffects.dock2)
						Object4Array.setFieldtype("dock2");
					else if (field.getFullEffects() == fullEffects.dock3)
						Object4Array.setFieldtype("dock3");
					else if (field.getFullEffects() == fullEffects.dock4)
						Object4Array.setFieldtype("dock4");
					else if (field.getFullEffects() == fullEffects.dock5)
						Object4Array.setFieldtype("dock5");
					else if (field.getFullEffects() == fullEffects.dock6)
						Object4Array.setFieldtype("dock6");
					else if (field.getFullEffects() == fullEffects.dock7)
						Object4Array.setFieldtype("dock7");
					else if (field.getFullEffects() == fullEffects.dock8) {
						Object4Array.setFieldtype("dock8");
					}

				}
				ArrayZeile.add(Object4Array);
				System.out.println("Zeile " + i + "\nArrayzeile: " + ArrayZeile.size());
			}
			JsonSaveArrayList.add(ArrayZeile);
		}

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try {
			// System.out.println("Json arra: " +JsonSaveArrayList.size());
			String json = gson.toJson(JsonSaveArrayList);
			FileWriter writer = new FileWriter(path);
			writer.write(json);
			writer.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	class JsonSaveObject {
		public JsonSaveObject() {
		}

		@SerializedName("fieldType")
		String fieldtype;
		@SerializedName("flag")
		int flag;
		@SerializedName("walls")
		int[] walls = { 0, 0, 0, 0 };
		@SerializedName("lasers")
		int[] lasers = { 0, 0, 0, 0 };
		@SerializedName("rotate")
		int[] rotate = { 0, 0 };
		@SerializedName("shift")
		int[] shift = { 0, 0, 0, 0 };

		public void setFieldtype(String fieldtype) {
			this.fieldtype = fieldtype;
		}

		public void setFlag(int flag) {
			this.flag = flag;
		}

		public void setWalls(int[] walls) {
			this.walls = walls;
		}

		public void setLasers(int[] lasers) {
			this.lasers = lasers;
		}

		public void setRotate(int[] rotate) {
			this.rotate = rotate;
		}

		public void setShift(int[] shift) {
			this.shift = shift;
		}

	}

	@Override
	public Object load(String path) throws FileNotFoundException, IOException {
		JsonReader jsonReader = new JsonReader(new FileReader(path));
		int k = 0;
		int l = 0;
		String name;
		JsonReader jsonReader2 = new JsonReader(new FileReader(path));
		int g = 0;
		int u = 0;
		
		
		jsonReader2.beginArray();
		while (jsonReader2.hasNext()) {
			jsonReader2.beginArray();
			while (jsonReader2.hasNext()) {
				jsonReader2.beginObject();
				while (jsonReader2.hasNext()) {
					jsonReader2.skipValue();
				}
				if(g==0)
					u++;
				jsonReader2.endObject();
			}
			g++;
			
			jsonReader2.endArray();

		}
		
		Field[][] course = new Field[g][u];
		
		jsonReader.beginArray();
		while (jsonReader.hasNext()) {
			jsonReader.beginArray();
			while (jsonReader.hasNext()) {
				jsonReader.beginObject();
				while (jsonReader.hasNext()) {

					name = jsonReader.nextName();

					if (name.equals("fieldType")) {
						fieldType = jsonReader.nextString();
					}
					if (name.equals("flag")) {
						flag = jsonReader.nextInt();
					}
					if (name.equals("walls")) {
						jsonReader.beginArray();

						for (int i = 0; i < 4; i++) {
							walls[i] = jsonReader.nextInt();
						}
						jsonReader.endArray();
					}
					if (name.equals("lasers")) {
						jsonReader.beginArray();
						for (int i = 0; i < 4; i++) {
							lasers[i] = jsonReader.nextInt();
						}
						jsonReader.endArray();
					}
					if (name.equals("rotate")) {
						jsonReader.beginArray();
						for (int i = 0; i < 2; i++) {
							rotate[i] = jsonReader.nextInt();
						}
						jsonReader.endArray();
					}
					if (name.equals("shift")) {
						jsonReader.beginArray();
						for (int i = 0; i < 4; i++) {

							shift[i] = jsonReader.nextInt();
						}
						jsonReader.endArray();
						// jsonReader.endObject();
					}

					if (flag != 0) {
						if (flag == 1)
							fullEffect = fullEffects.flag1;
						if (flag == 2)
							fullEffect = fullEffects.flag2;
						if (flag == 3)
							fullEffect = fullEffects.flag3;
						if (flag == 4)
							fullEffect = fullEffects.flag4;
					} else if (fieldType.equals("Repair")) {
						fullEffect = fullEffects.repair;
					} else if (fieldType.equals("Floor")) {
						fullEffect = fullEffects.stand;
					} else if (fieldType.equals("Pit")) {
						fullEffect = fullEffects.pit;
					} else if (fieldType.equals("dock1")) {
						fullEffect = fullEffects.dock1;
					} else if (fieldType.equals("dock2")) {
						fullEffect = fullEffects.dock2;
					} else if (fieldType.equals("dock3")) {
						fullEffect = fullEffects.dock3;
					} else if (fieldType.equals("dock4")) {
						fullEffect = fullEffects.dock4;
					} else if (fieldType.equals("dock5")) {
						fullEffect = fullEffects.dock5;
					} else if (fieldType.equals("dock6")) {
						fullEffect = fullEffects.dock6;
					} else if (fieldType.equals("dock7")) {
						fullEffect = fullEffects.dock7;
					} else if (fieldType.equals("dock8")) {
						fullEffect = fullEffects.dock8;
					} else if (shift[0] != 0 || shift[1] != 0 || shift[2] != 0 || shift[3] != 0) {
						if (shift[0] != 0) {
							fullEffect = fullEffects.belt_north;
						}
						if (shift[1] != 0) {
							fullEffect = fullEffects.belt_east;
						}
						if (shift[2] != 0) {
							fullEffect = fullEffects.belt_south;
						}
						if (shift[3] != 0) {
							fullEffect = fullEffects.belt_west;
						}
					}
					for (int i = 0; i < 4; i++) {
						if (walls[i] != 0 || lasers[i] != 0) {

							if (walls[i] != 0) {
								sideEffect[i] = sideEffects.wall;
							} else if (lasers[i] != 0) {
								sideEffect[i] = sideEffects.laser;
							} else {
								sideEffect[i] = sideEffects.stand;
							}
						}
						if (walls[i] == 0 && lasers[i] == 0) {
							sideEffect[i] = sideEffects.stand;
						}
					}

					course[l][k] = new Field(sideEffect[0], sideEffect[1], sideEffect[2], sideEffect[3], fullEffect);

				}
				k++;
				jsonReader.endObject();
			}
			jsonReader.endArray();
			k = 0;
			l++;

		}
		Course mapCourse = new Course(course);
		return mapCourse;
	}

}
