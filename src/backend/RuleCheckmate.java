package backend;

public class RuleCheckmate extends defaultRulebook {
    // fuer andere Funktionalitaeten *Checkmate*
	
	/**
	 * sets amoutFlags to the nessasary Array.
	 */
	
	
    int[] amountFlags = {0, 0};

    /**
     * Überschreiben der Methode für den Spielmodus Checkmate.
     *
     * @return boolean
     */
    @Override
    public boolean isStartPossible(Ringbuffer Playerbuffer) {
        if (Playerbuffer.getFreeSpace() >= 0 && Playerbuffer.getFreeSpace() <= 3) {
            return true;
        } else
            return false;
    }
    @Override
    public int[] getFlagNumber() {
    	return amountFlags;
    }
    
    @Override
    public boolean isPowerDownPossible() {
    	return true;
    }
}
